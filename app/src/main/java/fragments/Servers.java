package fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import adapters.AdapterForListServersTab;
import model.Database;
import model.ListBean;
import model.Server;

public class Servers extends Fragment {

    private ProgressDialog progressDialog;

    private static final String TAG = Servers.class.getSimpleName();
    private static final String TAG_NAME = "body";

    private SharedPreferences pref;
    String ipFromSharedPref;

    private static final String urlServers = "/MDBA/servlet?oper=servers&p1=";

    AdapterForListServersTab customListAdapterServers;
    private ArrayList<Server> listBeanItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_servers, container, false);

        pref = getActivity().getSharedPreferences("ip", getContext().MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip", "");

        listBeanItems = new ArrayList<Server>();
        ListView listView = (ListView) rootView.findViewById(R.id.servers_list);

        customListAdapterServers = new AdapterForListServersTab(getContext(), listBeanItems);
        listView.setAdapter(customListAdapterServers);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //showPDialog();

        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlServers,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                Server server = new Server();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                server.setId(objectPrvi.getInt("id"));
                                server.setIp(objectPrvi.getString("ip"));
                                server.setName(objectPrvi.getString("name"));
                                server.setUsername(objectPrvi.getString("username"));
                                server.setPassword(objectPrvi.getString("password"));
                                server.setPort(objectPrvi.getInt("port"));
                                server.setStatus(objectPrvi.getString("status"));
                                customListAdapterServers.add(server);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        customListAdapterServers.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        queue.add(jsonArrayRequest);
        return rootView;
    }
    public void showPDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getActivity().getResources().getString(R.string.load));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        hidePDialog();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
