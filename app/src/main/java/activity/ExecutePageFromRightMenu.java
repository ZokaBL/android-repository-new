package activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.AdapterForRightMenu;
import adapters.AdapterLeftMenuExecutePage;
import adapters.CustomListAdapterLMenu;
import appcontrol.AppController;
import model.LeftMenuBean;
import model.RightMenuBean;

/**
 * Created by zoka on 25.10.2016.
 */

public class ExecutePageFromRightMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //private static final String ip = "http://212.93.241.142:20282";

    String ipFromSharedPref;
    private SharedPreferences pref;

    // sve za left navigation drawer
    private static final String urlLeftMenu = "/MDBA/servlet?oper=menul";
    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<LeftMenuBean> listLeviMeniItems;
    AdapterLeftMenuExecutePage leftAdapter;
    LeftMenuBean listItems;

    //sve za right navigation drawer
    private ArrayList<RightMenuBean> listDesniMeniItems;
    private static final String urlRightMenu = "/MDBA/servlet?oper=menur";
    AdapterForRightMenu rightAdapter;
    RightMenuBean listItemsRight;
    private DrawerLayout drawer;
    String action1;

    ImageView iconr, iconLeft;
    EditText editText1;
    String idFromHomePage;
    Button sendButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.execute_page_from_right_menu);

        pref = getSharedPreferences("ip", MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");

        Intent getid = getIntent();
        action1 = getid.getStringExtra("action1");

        //Toast.makeText(getBaseContext(), action1, Toast.LENGTH_SHORT).show();

        idFromHomePage = getid.getStringExtra("idFromHomePage");


        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        iconr = (ImageView) findViewById(R.id.imgRightMenu);
        iconLeft = (ImageView) findViewById(R.id.imgLeftMenu);

        editText1 = (EditText) findViewById(R.id.action1);
        editText1.setText(action1);

        ListView openLeftMenuList = (ListView) findViewById(R.id.drawer_list_left);
        ListView openRightMenuList = (ListView) findViewById(R.id.drawer_list_right);

        sendButton = (Button) findViewById(R.id.executeButton1);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), editText1.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        iconr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);
                drawer.closeDrawer(GravityCompat.START);


            }

        });

        iconLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
                drawer.closeDrawer(GravityCompat.END);

            }

        });

        loadingLeftMenu();
        loadingRightMenu();

        openLeftMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {



            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (id == 0) {
                    if (listLeviMeniItems.get(0).getPage().equals("null")){
                        Intent main = new Intent(view.getContext(), MainActivity.class);
                        view.getContext().startActivity(main);
                        finish();
                    }
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        });


        openRightMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (id == 0) {
                    if (listLeviMeniItems.get(0).getPage().equals("null")) {
                        Intent main = new Intent(view.getContext(), MainActivity.class);
                        view.getContext().startActivity(main);
                        finish();
                    }
                }
                drawer.closeDrawer(GravityCompat.END);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

    }


    // metoda koja ucitava LEFT MENU
    public void loadingLeftMenu() {
        listLeviMeniItems = new ArrayList<LeftMenuBean>();
        ListView listView = (ListView) findViewById(R.id.drawer_list_left);
        leftAdapter = new AdapterLeftMenuExecutePage(this, listLeviMeniItems);
        listView.setAdapter(leftAdapter);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://"+ipFromSharedPref + urlLeftMenu,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                listItems = new LeftMenuBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                listItems.setP1(objectPrvi.getString("p1"));
                                listItems.setShortDecs(objectPrvi.getString("shortDecs"));
                                listItems.setOper(objectPrvi.getString("oper"));
                                listItems.setPage(objectPrvi.getString("page"));
                                listLeviMeniItems.add(listItems);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        leftAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }


    // metoda koja ucitava RIGHT MENU
    public void loadingRightMenu() {
        listDesniMeniItems = new ArrayList<RightMenuBean>();
        ListView listView = (ListView) findViewById(R.id.drawer_list_right);
        rightAdapter = new AdapterForRightMenu(this, listDesniMeniItems);
        listView.setAdapter(rightAdapter);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlRightMenu,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                listItemsRight = new RightMenuBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                listItemsRight.setShortDescRightMenu(objectPrvi.getString("shortDesc"));
                                listItemsRight.setAction1(objectPrvi.getString("action1"));
                                listDesniMeniItems.add(listItemsRight);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        rightAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == 0) {

            //viewPager.setCurrentItem(0);
        } else if (id == 1) {
            // viewPager.setCurrentItem(1);
        } else if (id == 2) {
            //viewPager.setCurrentItem(2);
        } else if (id == 3) {

        } else if (id == 4) {
            finish();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }
}
