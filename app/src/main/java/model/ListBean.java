package model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zoka on 23.1.2017.
 */

public class ListBean implements Parcelable {

    private String name;
    private String id;



    public ListBean() {}

    // Get and set
    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
    }


    // Creator
    public static final Parcelable.Creator<ListBean> CREATOR
            = new Parcelable.Creator<ListBean>() {
        public ListBean createFromParcel(Parcel in) {
            return new ListBean(in);
        }

        public ListBean[] newArray(int size) {
            return new ListBean[size];
        }
    };

    // "De-parcel object
    public ListBean(Parcel in) {
        name = in.readString();
        id = in.readString();
    }

    @Override
    public String toString() {
        return "Name:" + name + " ID: " + id;
        //return "Name:" + name + " Surname: " + surname + " IDX: " + idx;
    }
}
