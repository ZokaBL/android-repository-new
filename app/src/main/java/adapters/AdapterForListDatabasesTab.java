package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoka.app.R;
import java.util.ArrayList;

import activity.DatabasePage;
import model.Database;
import model.ListBean;

/**
 * Created by zoka on 25.1.2017.
 */

public class AdapterForListDatabasesTab extends ArrayAdapter<Database> {

   ViewHolder holder1;
    ArrayList<Database> items;
    ArrayList<String> typeDataBases;

    public AdapterForListDatabasesTab(Context context, ArrayList<Database> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Database database = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.databases_list_row, parent, false);
        }
        holder1 = new AdapterForListDatabasesTab.ViewHolder();
        holder1.shortDescAlarm =(TextView)convertView.findViewById(R.id.shortDescAlarms);
        holder1.id =(TextView)convertView.findViewById(R.id.id);
       // holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortDescAlarm.setText(database.getName());
        holder1.id.setText(String.valueOf(database.getId()));


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addDatabase = new Intent(getContext(), DatabasePage.class);
                String name = database.getName();
                String hostname = database.getHostname();
                String port = String.valueOf(database.getPort());
                String datab = database.getDatabase();
                int dbType = database.getDbType();

               // addDatabase.putExtra("position",position);

                addDatabase.putExtra("name",name);
                addDatabase.putExtra("hostname",hostname);
                addDatabase.putExtra("port",port);
                addDatabase.putExtra("database",datab);
                addDatabase.putExtra("dbtype",dbType);

                 /*addDatabase.putExtra("status",status);*/

        /*        ArrayList<String> typeDatabaseList = new ArrayList<String>();
                typeDatabaseList.add(typeDatabase);
                addDatabase.putExtra("typeDatabase", typeDatabaseList);





                ArrayList<String> statusList = new ArrayList<String>();
                statusList.add(status);
                addDatabase.putExtra("status", statusList);*/

                getContext().startActivity(addDatabase);
            }
        });

/*        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        return convertView;
    }

    class ViewHolder {
        // List on Databases
        TextView id;
        TextView shortDescAlarm;
        //ImageView imageView;

    }
}
