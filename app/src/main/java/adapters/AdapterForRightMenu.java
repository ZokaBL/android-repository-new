package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.zoka.app.R;

import java.util.ArrayList;


import activity.ExecutePage;
import activity.ExecutePageFromRightMenu;
import model.RightMenuBean;

/**
 * Created by Zoka on 1.9.2016.
 */
public class AdapterForRightMenu extends ArrayAdapter<RightMenuBean>
{
    ViewHolder holder1;
    Context context;

    public AdapterForRightMenu(Context context, ArrayList<RightMenuBean> items) {
        super(context, 0, items);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RightMenuBean desniMeniBean = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.single_data_right_menu, parent, false);
        }
        holder1=new ViewHolder();
        holder1.shortDescRightMenu=(TextView)convertView.findViewById(R.id.shortDescRightMenu);
        holder1.iv=(ImageView)convertView.findViewById(R.id.imageView);
        convertView.setTag(holder1);
        holder1.shortDescRightMenu.setText(desniMeniBean.getShortDescRightMenu());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    case 0:
                        //String action1 = desniMeniBean.getAction1();
                    Intent executePage = new Intent(v.getContext(), ExecutePageFromRightMenu.class);
                        executePage.putExtra("action1",desniMeniBean.getShortDescRightMenu());
                    v.getContext().startActivity(executePage);

                        ((Activity) getContext()).finish();

                        break;
                    case 1:
                        Intent executePage1 = new Intent(v.getContext(), ExecutePageFromRightMenu.class);
                        executePage1.putExtra("action1",desniMeniBean.getAction1());
                        v.getContext().startActivity(executePage1);

                        ((Activity) getContext()).finish();
                        break;
                    case 2:
                        Intent executePage2 = new Intent(v.getContext(), ExecutePageFromRightMenu.class);
                        executePage2.putExtra("action1",desniMeniBean.getAction1());
                        v.getContext().startActivity(executePage2);
                        ((Activity) getContext()).finish();
                        break;
                    case 3:
                        Intent executePage3 = new Intent(v.getContext(), ExecutePageFromRightMenu.class);
                        executePage3.putExtra("action1",desniMeniBean.getAction1());
                        v.getContext().startActivity(executePage3);
                        ((Activity) getContext()).finish();
                        break;
                    case 4:
                        Intent executePage4 = new Intent(v.getContext(), ExecutePageFromRightMenu.class);
                        executePage4.putExtra("action1",desniMeniBean.getAction1());
                        v.getContext().startActivity(executePage4);
                        ((Activity) getContext()).finish();
                        break;

                    //selectItem(position);


                }

                //Toast.makeText(getContext(), "Kliknuo si " + position, Toast.LENGTH_SHORT).show();
            }
        });



        return convertView;
    }

    class ViewHolder {
        TextView shortDescRightMenu;
        ImageView iv;
    }

}

