package model;



import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class Server {
    private int id;
    private String ip;
    private String name;
    private String username;
    private String password;
    private int port;
    private String status;

    public Server() {
    }

    public Server(int id, String ip, String name, String username, String password, int port, String status) {
        this.id = id;
        this.ip = ip;
        this.name = name;
        this.username = username;
        this.password = password;
        this.port = port;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString(){
        String jsonString = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;

    }
}
