package model;

/**
 * Created by zoka on 28.10.2016.
 */

public class ObjectPageBean {

    // Footer
    private String p1Footer;
    private String p2Footer;
    private String footerButton;
    private String footerAction;
    private String pageName;

    public ObjectPageBean(String p1Footer, String p2Footer, String footerButton, String footerAction, String pageName) {
        this.p1Footer = p1Footer;
        this.p2Footer = p2Footer;
        this.footerButton = footerButton;
        this.footerAction = footerAction;
        this.pageName = pageName;
    }


    public String getP1Footer() {
        return p1Footer;
    }

    public void setP1Footer(String p1Footer) {
        this.p1Footer = p1Footer;
    }

    public String getP2Footer() {
        return p2Footer;
    }

    public void setP2Footer(String p2Footer) {
        this.p2Footer = p2Footer;
    }

    public String getFooterButton() {
        return footerButton;
    }

    public String setFooterButton(String footerButton) {
        this.footerButton = footerButton;
        return footerButton;
    }

    public String getFooterAction() {
        return footerAction;
    }

    public void setFooterAction(String footerAction) {
        this.footerAction = footerAction;
    }

    public String getPageName() {
        return pageName;
    }

    public String setPageName(String pageName) {
        this.pageName = pageName;
        return pageName;
    }



    // Header

    private String headerButton;
    private String p1Header;
    private String p2Header;
    private String headerHint;
    private String headerCaption;
    private String headerAction;
    private String headerText;


    public String getHeaderButton() {
        return headerButton;
    }

    public void setHeaderButton(String headerButton) {
        this.headerButton = headerButton;
    }

    public String getP1Header() {
        return p1Header;
    }

    public void setP1Header(String p1Header) {
        this.p1Header = p1Header;
    }

    public String getP2Header() {
        return p2Header;
    }

    public void setP2Header(String p2Header) {
        this.p2Header = p2Header;
    }

    public String getHeaderHint() {
        return headerHint;
    }

    public void setHeaderHint(String headerHint) {
        this.headerHint = headerHint;
    }

    public String getHeaderCaption() {
        return headerCaption;
    }

    public void setHeaderCaption(String headerCaption) {
        this.headerCaption = headerCaption;
    }

    public String getHeaderAction() {
        return headerAction;
    }

    public void setHeaderAction(String headerAction) {
        this.headerAction = headerAction;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    // Dropdown list
    private String ID;
    private String nameDataBase;


    public ObjectPageBean() {
    }

    public ObjectPageBean(String ID, String nameDataBase) {
        this.ID = ID;
        this.nameDataBase = nameDataBase;
    }

    public String getNameDataBase() {

        return nameDataBase;
    }

    public String setNameDataBase(String nameDataBase) {
        this.nameDataBase = nameDataBase;
        return nameDataBase;
    }

    public String getID() {

        return ID;
    }
    public String setID(String iD) {
        ID = iD;
        return iD;
    }

    @Override
    public String toString() {
        return ID;
    }


    // page

    private String gotopage;

    public String getGotopage() {
        return gotopage;
    }

    public void setGotopage(String gotopage) {
        this.gotopage = gotopage;
    }

    // body

    private String p1Body;
    private String p2Body;
    private String size;
    private String hidden;
    private String hint;
    private String name;
    private String caption;
    private String type;
    private String value;
    private String required;

    public ObjectPageBean(String p1Footer, String p2Footer, String footerButton, String footerAction, String pageName, String headerButton, String p1Header, String p2Header, String headerHint, String headerCaption, String headerAction, String headerText, String ID, String nameDataBase, String p1Body, String p2Body, String size, String hidden, String hint, String name, String caption, String type, String value, String required) {
        this.p1Footer = p1Footer;
        this.p2Footer = p2Footer;
        this.footerButton = footerButton;
        this.footerAction = footerAction;
        this.pageName = pageName;
        this.headerButton = headerButton;
        this.p1Header = p1Header;
        this.p2Header = p2Header;
        this.headerHint = headerHint;
        this.headerCaption = headerCaption;
        this.headerAction = headerAction;
        this.headerText = headerText;
        this.ID = ID;
        this.nameDataBase = nameDataBase;
        this.p1Body = p1Body;
        this.p2Body = p2Body;
        this.size = size;
        this.hidden = hidden;
        this.hint = hint;
        this.name = name;
        this.caption = caption;
        this.type = type;
        this.value = value;
        this.required = required;
    }

    public String getP1Body() {
        return p1Body;
    }

    public void setP1Body(String p1Body) {
        this.p1Body = p1Body;
    }

    public String getP2Body() {
        return p2Body;
    }

    public void setP2Body(String p2Body) {
        this.p2Body = p2Body;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }
}