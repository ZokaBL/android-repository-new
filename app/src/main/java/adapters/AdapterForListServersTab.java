package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zoka.app.R;
import java.util.ArrayList;

import activity.DatabasePage;
import activity.ServersPage;
import model.ListBean;
import model.Server;

/**
 * Created by zoka on 25.1.2017.
 */

public class AdapterForListServersTab extends ArrayAdapter<Server> {

    ViewHolder holder1;

    public AdapterForListServersTab(Context context, ArrayList<Server> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Server server = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.servers_list_row, parent, false);
        }
        holder1 = new AdapterForListServersTab.ViewHolder();
        holder1.shortDescAlarm =(TextView)convertView.findViewById(R.id.shortDescAlarms);
        holder1.id =(TextView)convertView.findViewById(R.id.id);
       // holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortDescAlarm.setText(server.getName());
        holder1.id.setText(String.valueOf(server.getId()));


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addServer = new Intent(getContext(), ServersPage.class);

                String id = String.valueOf(server.getId());
                String ip = server.getIp();
                String name = server.getName();
                String username = server.getUsername();
                String password = server.getPassword();
                String port = String.valueOf(server.getPort());

                addServer.putExtra("id",id);
                addServer.putExtra("ip",ip);
                addServer.putExtra("name",name);
                addServer.putExtra("username",username);
                addServer.putExtra("password",password);
                addServer.putExtra("port",port);

                getContext().startActivity(addServer);
            }
        });

        return convertView;
    }

    class ViewHolder {
        // List on Servers
        TextView id;
        TextView shortDescAlarm;
       // ImageView imageView;

    }
}
