package model;

/**
 * Created by zoka on 12.10.2016.
 */

public class ResolveAndCheckBean {

    private String shortAlarm;
    private String description;
    private String objectName;
    private String objectOwner;

    private String action1;
    private String action2;
    private String action3;

    public ResolveAndCheckBean() {
    }

    public ResolveAndCheckBean(String shortAlarm, String description, String objectName, String objectOwner, String action1, String action2, String action3) {
        this.shortAlarm = shortAlarm;
        this.description = description;
        this.objectName = objectName;
        this.objectOwner = objectOwner;
        this.action1 = action1;
        this.action2 = action2;
        this.action3 = action3;
    }

    public String getShortAlarm() {
        return shortAlarm;
    }

    public void setShortAlarm(String shortAlarm) {
        this.shortAlarm = shortAlarm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectOwner() {
        return objectOwner;
    }

    public void setObjectOwner(String objectOwner) {
        this.objectOwner = objectOwner;
    }

    public String getAction1() {
        return action1;
    }

    public void setAction1(String action1) {
        this.action1 = action1;
    }

    public String getAction2() {
        return action2;
    }

    public void setAction2(String action2) {
        this.action2 = action2;
    }

    public String getAction3() {
        return action3;
    }

    public void setAction3(String action3) {
        this.action3 = action3;
    }
}
