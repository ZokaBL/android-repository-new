package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import activity.AlarmDetails;
import activity.ExecutePage;
import com.zoka.app.R;
import model.HomePageAlarmsBean;

/**
 * Created by Zoka on 20.9.2016.
 */
public class AdapterForListAlarmsHomePage extends ArrayAdapter<HomePageAlarmsBean> {

    ViewHolder holder1;

    public AdapterForListAlarmsHomePage(Context context, ArrayList<HomePageAlarmsBean> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final HomePageAlarmsBean homePageBean = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.home_page_list_row, parent, false);
        }
        holder1 = new ViewHolder();
        holder1.shortDescAlarm =(TextView)convertView.findViewById(R.id.shortDescAlarms);
        holder1.id =(TextView)convertView.findViewById(R.id.id);
        holder1.action1 =(TextView)convertView.findViewById(R.id.action1);
        holder1.action2 =(TextView)convertView.findViewById(R.id.action2);
        holder1.action3 =(TextView)convertView.findViewById(R.id.action3);
        holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortDescAlarm.setText(homePageBean.getShortDescAlarms());
         holder1.id.setText(homePageBean.getId());
         holder1.action1.setText(homePageBean.getAction1());
        holder1.action2.setText(homePageBean.getAction2());
        holder1.action3.setText(homePageBean.getAction3());

        holder1.shortDescAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = homePageBean.getId();
                Intent alarmDetails = new Intent(getContext(), AlarmDetails.class);
                alarmDetails.putExtra("id",id);
                getContext().startActivity(alarmDetails);

            }
        });

        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String action3 = homePageBean.getAction3();
                if(action3.equals(null)|| action3.equals("")){
                    Intent executePage = new Intent(v.getContext(), ExecutePage.class);
                    executePage.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    executePage.putExtra("idFromHomePage",homePageBean.getId());
                    getContext().startActivity(executePage);
                }else{
                    Toast.makeText(getContext(), "action3 is not null", Toast.LENGTH_LONG).show();
                }
            }
        });
        return convertView;
    }

    class ViewHolder {
        // Alarms on HomePage
        TextView id;
        TextView action1;
        TextView action2;
        TextView action3;
        TextView shortDescAlarm;
        ImageView imageView;

    }
}

