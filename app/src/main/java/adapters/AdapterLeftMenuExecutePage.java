package adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoka.app.R;
import java.util.ArrayList;
import model.LeftMenuBean;

/**
 * Created by zoka on 18.10.2016.
 */

public class AdapterLeftMenuExecutePage extends ArrayAdapter<LeftMenuBean>{
    //ViewHolder holder1;

    ArrayList<LeftMenuBean> animalList = new ArrayList<>();

    public AdapterLeftMenuExecutePage(Context context, ArrayList<LeftMenuBean> items) {
        super(context, 0, items);
        animalList = items;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final LeftMenuBean leviMeniBean = getItem(position);
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.single_data_left_menu, null);
        TextView textView = (TextView) view.findViewById(R.id.shortDecs);
        ImageView imageView = (ImageView) view.findViewById(R.id.imgView);
        textView.setText(leviMeniBean.getShortDecs());
        imageView.setImageResource(leviMeniBean.getImage());

    /*    textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // doing something

            }
        });*/

        return view;
    }





}

