package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zoka.app.R;

import java.util.ArrayList;

import activity.AlarmDetails;
//import activity.AlarmDetailsTest;
import activity.ExecutePage;
import activity.ExecutePageWithoutTabs;
import fragments.HomePage;
import model.Test;

/**
 * Created by zoka on 11.10.2016.
 */

public class CustomListAdapterLMenu extends ArrayAdapter<Test> implements AdapterView.OnItemClickListener {
    ViewHolder holder1;
    //private static final String urlMessage = "http://212.93.241.142:20282/MDBA/servlet?oper=exec&cmd=test";
    private static final String TAG = HomePage.class.getSimpleName();

    public CustomListAdapterLMenu(Context context, ArrayList<Test> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Test test = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.home_page_list_row, parent, false);
        }
        holder1 = new ViewHolder();
        holder1.shortDescAlarm =(TextView)convertView.findViewById(R.id.shortDescAlarms);
        holder1.id =(TextView)convertView.findViewById(R.id.id);
        holder1.action1 =(TextView)convertView.findViewById(R.id.action1);
        holder1.action2 =(TextView)convertView.findViewById(R.id.action2);
        holder1.action3 =(TextView)convertView.findViewById(R.id.action3);
        holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortDescAlarm.setText(test.getShortDescAlarms());
        holder1.id.setText(test.getId());
        holder1.action1.setText(test.getAction1());
        holder1.action2.setText(test.getAction2());
        holder1.action3.setText(test.getAction3());
        //Picasso.with(getContext()).load(leviMeniBean.getImage()).into(holder1.iv);

        holder1.shortDescAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = test.getId();
                Intent alarmDetails = new Intent(v.getContext(), AlarmDetails.class);
                alarmDetails.putExtra("id",id);
                v.getContext().startActivity(alarmDetails);

               // Toast.makeText(getContext(), "Sta se ovde otvara?", Toast.LENGTH_LONG).show();

            }
        });

        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String action3 = test.getAction3();
                if(action3.equals(null)|| action3.equals("")){
                    //String id = homePageBean.getId();
                    Intent alarmDetails = new Intent(v.getContext(), ExecutePage.class);
                    //alarmDetails.putExtra("id",id);
                    v.getContext().startActivity(alarmDetails);

                }else{
                    //showMessage();
                    //String alarmId = homePageBean.getId();
                    Toast.makeText(getContext(), "action3 is not null", Toast.LENGTH_LONG).show();
                    //Toast.makeText(getContext(), alarmId + "" + action3, Toast.LENGTH_LONG).show();

                  /*  Intent alarmDetails = new Intent(getContext(), AlarmDetails.class);
                    getContext().startActivity(alarmDetails);*/
                }
            }
        });
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

       /*         Intent alarmDetails = new Intent(getContext(), AlarmDetailsTest.class);
                //alarmDetails.putExtra("id",id);
                getContext().startActivity(alarmDetails);*/
        Toast.makeText(getContext(), "Sta se ovde otvara?", Toast.LENGTH_LONG).show();

    }

    class ViewHolder {
        TextView id;
        TextView action1;
        TextView action2;
        TextView action3;
        TextView shortDescAlarm;
        ImageView imageView;

    }



/*    private void showMessage() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlMessage, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    HomePageAlarmsBean homePageBean = new HomePageAlarmsBean();
                    String msg = homePageBean.setMessage(response.getString("message"));
                    Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }*/

}

