package activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.AdapterForLeftMenu;
import adapters.AdapterForRightMenu;
import adapters.AdapterLeftMenuExecutePage;
import adapters.CustomListAdapterLMenu;
import appcontrol.AppController;
import model.ExecutePageBean;
import model.LeftMenuBean;
import model.RightMenuBean;

/**
 * Created by zoka on 6.10.2016.
 */

public class ExecutePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    String ipFromSharedPref;
    private SharedPreferences pref;

    private static  final String urlExecutePageStart = "/MDBA/servlet?oper=execpage&id=";
    private static  final String urlExecutePageEnd ="&action3=&p1=&p2=";

    // left navigation drawer
    private static final String urlLeftMenu = "/MDBA/servlet?oper=menul";
    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<LeftMenuBean> listLeviMeniItems;
    AdapterLeftMenuExecutePage leftAdapter;
    LeftMenuBean listItems;

    //right navigation drawer
    private ArrayList<RightMenuBean> listDesniMeniItems;
    private static final String urlRightMenu = "/MDBA/servlet?oper=menur";
    AdapterForRightMenu rightAdapter;
    RightMenuBean listItemsRight;
    private DrawerLayout drawer;
    String action1;

    ImageView iconr, iconLeft;
    EditText editText1;
    Button sendButton;

    String idFromHomePage;
    String plainTxt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.execute_page);

        pref = getSharedPreferences("ip", MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");

        Intent getid = getIntent();
        action1 = getid.getStringExtra("action1");
        idFromHomePage = getid.getStringExtra("idFromHomePage");



        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        iconr = (ImageView) findViewById(R.id.imgRightMenu);
        iconLeft = (ImageView) findViewById(R.id.imgLeftMenu);
        editText1 = (EditText) findViewById(R.id.action1);
        editText1.setText(action1);
        ListView openLeftMenuList = (ListView) findViewById(R.id.drawer_list_left);
        ListView openRightMenuList = (ListView) findViewById(R.id.drawer_list_right);

        sendButton = (Button) findViewById(R.id.executeButton1);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Klik", Toast.LENGTH_SHORT).show();
            }
        });


        iconr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.END);
                drawer.closeDrawer(GravityCompat.START);
            }

        });

        iconLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
                drawer.closeDrawer(GravityCompat.END);
            }

        });

        loadingExecutePage();
        loadingLeftMenu();
        loadingRightMenu();


        openLeftMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    if (listLeviMeniItems.get(0).getPage().equals("null")){
                        Intent main = new Intent(view.getContext(), MainActivity.class);
                        view.getContext().startActivity(main);
                        finish();
                    }
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        });
                openRightMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    if (listLeviMeniItems.get(0).getPage().equals("null")) {
                        Intent main = new Intent(view.getContext(), MainActivity.class);
                        view.getContext().startActivity(main);
                        finish();
                    }
                }
                drawer.closeDrawer(GravityCompat.END);
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

    }


    // ucitavanje levog menija
    public void loadingLeftMenu() {
        listLeviMeniItems = new ArrayList<LeftMenuBean>();
        ListView listView = (ListView) findViewById(R.id.drawer_list_left);
        leftAdapter = new AdapterLeftMenuExecutePage(this, listLeviMeniItems);
        listView.setAdapter(leftAdapter);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlLeftMenu,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                listItems = new LeftMenuBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                listItems.setP1(objectPrvi.getString("p1"));
                                listItems.setShortDecs(objectPrvi.getString("shortDecs"));
                                listItems.setOper(objectPrvi.getString("oper"));
                                listItems.setPage(objectPrvi.getString("page"));
                                listItems.setImageName(objectPrvi.getString("imageName"));

                                if (listItems.getImageName().equals("home")){
                                    listItems.setImage(R.drawable.home);
                                }if (listItems.getImageName().equals("relations")){
                                    listItems.setImage(R.drawable.relations);
                                }
                                if (listItems.getImageName().equals("invalid")){
                                    listItems.setImage(R.drawable.invalid);
                                }
                                if (listItems.getImageName().equals("servers2")){
                                    listItems.setImage(R.drawable.servers2);
                                }
                                if (listItems.getImageName().equals("schedulerjobs")){
                                    listItems.setImage(R.drawable.schedulerjobs);
                                }
                                if (listItems.getImageName().equals("jobs")){
                                    listItems.setImage(R.drawable.jobs);
                                }if (listItems.getImageName().equals("users")){
                                    listItems.setImage(R.drawable.users);
                                }
                                if (listItems.getImageName().equals("roles")){
                                    listItems.setImage(R.drawable.roles);
                                }
                                if (listItems.getImageName().equals("triggers")){
                                    listItems.setImage(R.drawable.triggers);
                                }
                                if (listItems.getImageName().equals("sequences")){
                                    listItems.setImage(R.drawable.sequences);
                                }
                                if (listItems.getImageName().equals("tables")){
                                    listItems.setImage(R.drawable.tables);
                                }
                                if (listItems.getImageName().equals("views")){
                                    listItems.setImage(R.drawable.views);
                                }
                                if (listItems.getImageName().equals("dblinks")){
                                    listItems.setImage(R.drawable.dblinks);
                                }
                                if (listItems.getImageName().equals("tablespaces")){
                                    listItems.setImage(R.drawable.tablespaces);
                                }
                                if (listItems.getImageName().equals("favourites")){
                                    listItems.setImage(R.drawable.favourites);
                                }
                                if (listItems.getImageName().equals("indexes2")){
                                    listItems.setImage(R.drawable.indexes2);
                                }
                                if (listItems.getImageName().equals("indexes")){
                                    listItems.setImage(R.drawable.indexes);
                                }
                                if (listItems.getImageName().equals("home2")){
                                    listItems.setImage(R.drawable.home2);
                                }
                                if (listItems.getImageName().equals("databases")){
                                    listItems.setImage(R.drawable.databases);
                                }
                                if (listItems.getImageName().equals("settings")){
                                    listItems.setImage(R.drawable.settings);
                                }
                                if (listItems.getImageName().equals("help")){
                                    listItems.setImage(R.drawable.help);
                                }
                                if (listItems.getImageName().equals("lists")){
                                    listItems.setImage(R.drawable.lists);
                                }
                                if (listItems.getImageName().equals("settings2")){
                                    listItems.setImage(R.drawable.settings2);
                                }
                                if (listItems.getImageName().equals("packages")){
                                    listItems.setImage(R.drawable.packages);
                                }
                                if (listItems.getImageName().equals("servers")){
                                    listItems.setImage(R.drawable.servers);
                                }
                                if (listItems.getImageName().equals("settings3")){
                                    listItems.setImage(R.drawable.settings3);
                                }
                                if (listItems.getImageName().equals("documents")){
                                    listItems.setImage(R.drawable.documents);
                                }
                                //listItems.setImage(R.drawable.icon_servers);
                                listLeviMeniItems.add(listItems);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        leftAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }
    // ucitavanje RIGHT MENU
    public void loadingRightMenu() {
        listDesniMeniItems = new ArrayList<RightMenuBean>();
        ListView listView = (ListView) findViewById(R.id.drawer_list_right);
        rightAdapter = new AdapterForRightMenu(this, listDesniMeniItems);
        listView.setAdapter(rightAdapter);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlRightMenu,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                listItemsRight = new RightMenuBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                listItemsRight.setShortDescRightMenu(objectPrvi.getString("shortDesc"));
                                listItemsRight.setAction1(objectPrvi.getString("action1"));
                                listDesniMeniItems.add(listItemsRight);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        rightAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }


    // ucitavanje EXECUTE PAGE
    public void loadingExecutePage() {
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlExecutePageStart + idFromHomePage + urlExecutePageEnd,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONArray jsonArray = response.getJSONArray(i);
                                JSONObject obj = (JSONObject) jsonArray.get(i);
                                EditText editTextExecutePage = (EditText) findViewById(R.id.action1);
                                String hint1 = obj.getString("hint1");
                                if (hint1.equals("null")||hint1.equals("")){
                                    editTextExecutePage.setVisibility(View.INVISIBLE);

                                }else {
                                    editTextExecutePage.setVisibility(View.VISIBLE);
                                    editTextExecutePage.setHint(hint1);
                                }

                                Button executeButton1 = (Button) findViewById(R.id.executeButton1);
                                executeButton1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Toast.makeText(getBaseContext(), "klik", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                String buttonText = obj.getString("button1");
                                if (buttonText.equals("null")||buttonText.equals("")){
                                    executeButton1.setVisibility(View.INVISIBLE);
                                }else {
                                    executeButton1.setVisibility(View.VISIBLE);
                                    executeButton1.setText(buttonText);
                                }

                                TextView plainText = (TextView) findViewById(R.id.plainText2);
                                plainTxt2 = obj.getString("text2");
                                if (plainTxt2.equals("null")||plainTxt2.equals("")){
                                    plainText.setVisibility(View.INVISIBLE);
                                }else{
                                    plainText.setVisibility(View.VISIBLE);
                                    plainText.setText(plainTxt2);
                                }

                                Button executeButton2 = (Button) findViewById(R.id.executeButton2);
                                String buttonText2 = obj.getString("button2");
                                if (buttonText2.equals("null") || buttonText2.equals("")){
                                    executeButton2.setVisibility(View.INVISIBLE);
                                }else {
                                    executeButton2.setVisibility(View.VISIBLE);
                                    executeButton2.setText(buttonText2);
                                }

                                TextView plainText3 = (TextView) findViewById(R.id.plainText3);
                                String plainTxt3 = obj.getString("text3");
                                if (plainTxt3.equals("null")|| plainTxt3.equals("")){
                                    plainText3.setVisibility(View.INVISIBLE);
                                }else {
                                    plainText3.setVisibility(View.VISIBLE);
                                    plainText3.setText(plainTxt3);
                                }

                                Button executeButton3 = (Button) findViewById(R.id.executeButton3);
                                String buttonText3 = obj.getString("button3");
                                if (buttonText3.equals("null") || buttonText3.equals("")){
                                    executeButton3.setVisibility(View.INVISIBLE);
                                }else {
                                    executeButton3.setVisibility(View.VISIBLE);
                                    executeButton3.setText(buttonText3);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == 0) {
            //viewPager.setCurrentItem(0);
        } else if (id == 1) {
           // viewPager.setCurrentItem(1);
        } else if (id == 2) {
            //viewPager.setCurrentItem(2);
        } else if (id == 3) {

        } else if (id == 4) {
            finish();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }
}
