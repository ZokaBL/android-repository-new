package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zoka.app.R;
import java.util.ArrayList;
import java.util.List;

import model.ListBean;

/**
 * Created by zoka on 23.1.2017.
 */

public class AdapterForListActivity extends ArrayAdapter<ListBean> {

    ViewHolder holder1;

    public AdapterForListActivity(Context context, List<ListBean> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ListBean listBean = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.single_item_list_activity, parent, false);
        }
        holder1 = new AdapterForListActivity.ViewHolder();

        holder1.id =(TextView)convertView.findViewById(R.id.id);
        holder1.name =(TextView)convertView.findViewById(R.id.nameInList);
        holder1.imageView =(ImageView)convertView.findViewById(R.id.image);


        convertView.setTag(holder1);
        //holder1.id.setText(listBean.getId());
        holder1.name.setText(listBean.getName());



        holder1.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return convertView;
    }

    class ViewHolder {
        // List in ListActivity
        TextView id;
        TextView name;
        ImageView imageView;

    }
}
