package model;




import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;



public class Database {

    private int id;
    private String name;
    private String hostname;
    private int port;
    private String database;
    private int dbType;
    private String status;
    private Server server;


    public Database() {
    }

    public Database(int id, String name, String hostname, int port, String database, int dbType, String status, Server server) {
        this.id = id;
        this.name = name;
        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.dbType = dbType;
        this.status = status;
        this.server = server;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }



    public String getStatus() {
        return status;
    }

    public String setStatus(String status) {
        this.status = status;
        return status;
    }


    public int getDbType() {
        return dbType;
    }

    public void setDbType(int dbType) {
        this.dbType = dbType;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public String toString(){
        String jsonString = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;

    }


}

