package model;

/**
 * Created by Zoka on 1.9.2016.
 */
public class RightMenuBean {


    // right menu niz 1
    private String shortDescRightMenu;
    private String action1;
    private String action2;
    private String p1;
    private String p2;

    // execute page niz 0
    private String hint1;
    private String p1ExecutePage;
    private String p2ExecutePage;
    private String button2;
    private String button3;
    private String text3;
    private String text1;
    private String text2;
    private String page;
    private String id;
    private String executePageAction1;
    private String executePageAction2;
    private String button1;
    private String executePageAction3;



    public RightMenuBean() {
    }

    public RightMenuBean(String shortDescRightMenu, String action1, String action2, String p1, String p2, String hint1, String p1ExecutePage, String p2ExecutePage, String button2, String button3, String text3, String text1, String text2, String page, String id, String executePageAction1, String executePageAction2, String button1, String executePageAction3) {
        this.shortDescRightMenu = shortDescRightMenu;
        this.action1 = action1;
        this.action2 = action2;
        this.p1 = p1;
        this.p2 = p2;
        this.hint1 = hint1;
        this.p1ExecutePage = p1ExecutePage;
        this.p2ExecutePage = p2ExecutePage;
        this.button2 = button2;
        this.button3 = button3;
        this.text3 = text3;
        this.text1 = text1;
        this.text2 = text2;
        this.page = page;
        this.id = id;
        this.executePageAction1 = executePageAction1;
        this.executePageAction2 = executePageAction2;
        this.button1 = button1;
        this.executePageAction3 = executePageAction3;
    }

    public String getShortDescRightMenu() {
        return shortDescRightMenu;
    }

    public void setShortDescRightMenu(String shortDescRightMenu) {
        this.shortDescRightMenu = shortDescRightMenu;
    }

    public String getAction1() {
        return action1;
    }

    public void setAction1(String action1) {
        this.action1 = action1;
    }

    public String getAction2() {
        return action2;
    }

    public void setAction2(String action2) {
        this.action2 = action2;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public String getP2() {
        return p2;
    }

    public void setP2(String p2) {
        this.p2 = p2;
    }

    public String getHint1() {
        return hint1;
    }

    public void setHint1(String hint1) {
        this.hint1 = hint1;
    }

    public String getP1ExecutePage() {
        return p1ExecutePage;
    }

    public void setP1ExecutePage(String p1ExecutePage) {
        this.p1ExecutePage = p1ExecutePage;
    }

    public String getP2ExecutePage() {
        return p2ExecutePage;
    }

    public void setP2ExecutePage(String p2ExecutePage) {
        this.p2ExecutePage = p2ExecutePage;
    }

    public String getButton2() {
        return button2;
    }

    public void setButton2(String button2) {
        this.button2 = button2;
    }

    public String getButton3() {
        return button3;
    }

    public void setButton3(String button3) {
        this.button3 = button3;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExecutePageAction1() {
        return executePageAction1;
    }

    public void setExecutePageAction1(String executePageAction1) {
        this.executePageAction1 = executePageAction1;
    }

    public String getExecutePageAction2() {
        return executePageAction2;
    }

    public void setExecutePageAction2(String executePageAction2) {
        this.executePageAction2 = executePageAction2;
    }

    public String getButton1() {
        return button1;
    }

    public void setButton1(String button1) {
        this.button1 = button1;
    }

    public String getExecutePageAction3() {
        return executePageAction3;
    }

    public void setExecutePageAction3(String executePageAction3) {
        this.executePageAction3 = executePageAction3;
    }

    @Override
    public String toString() {
        return "RightMenuBean{" +
                "shortDescRightMenu='" + shortDescRightMenu + '\'' +
                ", action1='" + action1 + '\'' +
                ", action2='" + action2 + '\'' +
                ", p1='" + p1 + '\'' +
                ", p2='" + p2 + '\'' +
                '}';
    }
}
