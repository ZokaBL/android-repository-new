package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zoka.app.R;

import java.util.ArrayList;
import java.util.List;

import model.Database;
import model.DatabasesTypeList;

/**
 * Created by zoka on 8.4.2017.
 */

public class AdapterForSpinerTypeDatabase extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<DatabasesTypeList> listData;
    private Context context;
    public AdapterForSpinerTypeDatabase(Context context, ArrayList<DatabasesTypeList> listData) {
        this.context = context;
        layoutInflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = listData;
    }
    @Override
    public int getCount() {
        return listData.size();
    }
    @Override
    public Object getItem(int position) {
        return (DatabasesTypeList)listData.get(position);
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AdapterForSpinerTypeDatabase.ViewHolder spinnerHolder;
        if(convertView == null){
            spinnerHolder = new AdapterForSpinerTypeDatabase.ViewHolder();
            convertView = layoutInflater.inflate(R.layout.spinner_list, parent, false);
            spinnerHolder.spinnerItemList = (TextView)convertView.findViewById(R.id.spinner_list_item);

            spinnerHolder.spinnerItemList.setTextSize(14);
            convertView.setTag(spinnerHolder);
        }else{
            spinnerHolder = (AdapterForSpinerTypeDatabase.ViewHolder)convertView.getTag();
        }

        spinnerHolder.spinnerItemList.setText(listData.get(position).getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
        //textview.setTextColor(Color.parseColor("#46629E"));
        textview.setPadding(8, 8, 8, 8);
        textview.setText(listData.get(position).getName());

        return textview;
    }
    class ViewHolder{
        TextView spinnerItemList;


    }
}