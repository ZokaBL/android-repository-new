package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zoka.app.R;

import activity.AboutActivity;
import activity.LoginActivity;
import activity.SettingsActivity;

/**
 * Created by zoka on 6.10.2016.
 */

public class Settings extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_logout_settings, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView) view.findViewById(R.id.list);
        String[] settingsItems = getResources().getStringArray(R.array.items_value_in_settings);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, settingsItems);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0:
                        Intent settings = new Intent(getActivity(), SettingsActivity.class);
                        startActivity(settings);
                        getActivity().finish();
                        break;
                    case 1:
                        Intent login = new Intent(getActivity(),LoginActivity.class);
                        startActivity(login);
                        getActivity().finish();
                        break;
                    case 2:
                        Intent about = new Intent(getActivity(), AboutActivity.class);
                        startActivity(about);
                        getActivity().finish();
                        break;
                }
            }
        });
    }
}

