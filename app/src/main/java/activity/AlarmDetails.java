package activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapters.AdapterForListAlarmDetails;
import com.zoka.app.R;
import model.AlarmDetailsBean;


public class AlarmDetails extends AppCompatActivity {


    String ipFromSharedPref;
    private SharedPreferences pref;

    ProgressDialog progressDialog;
    String id;
    private static final String urlAlarmDetails = "/MDBA/servlet?oper=alarmdetails&id=";
    private ArrayList<AlarmDetailsBean> listAlarmDetailsItems;
    AdapterForListAlarmDetails customListAdapterAlarmDetails;
    private static final String TAG = AlarmDetails.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_details_activity);

        pref = getSharedPreferences("ip", MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.alarm_details_activity);
        setSupportActionBar(toolbar);
        Intent getid = getIntent();
        id = getid.getStringExtra("id");
        loadingAlarmDetails();

    }

    public void loadingAlarmDetails() {
        listAlarmDetailsItems = new ArrayList<AlarmDetailsBean>();
        ListView listView = (ListView) findViewById(R.id.alarm_details_list_view);
        customListAdapterAlarmDetails = new AdapterForListAlarmDetails(this, listAlarmDetailsItems);
        listView.setAdapter(customListAdapterAlarmDetails);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        showPDialog();
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlAlarmDetails + id,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();
                        try {
                            for (int i=0; i<response.length(); i++) {
                                AlarmDetailsBean alarmDetailsBean = new AlarmDetailsBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                String hintMessage = alarmDetailsBean.setHint(objectPrvi.getString("hint1"));
                                EditText inputText = (EditText) findViewById(R.id.inputText);
                                inputText.setHint(hintMessage);
                                alarmDetailsBean.setShortDesc(objectPrvi.getString("shortDesc"));

                                String buttonText = alarmDetailsBean.setButton1(objectPrvi.getString("button1"));
                                Button btn = (Button) findViewById(R.id.button);
                                btn.setText(buttonText);

                                customListAdapterAlarmDetails.add(alarmDetailsBean);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        customListAdapterAlarmDetails.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        queue.add(jsonArrayRequest);
    }

    public void showPDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(this.getResources().getString(R.string.load));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        hidePDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
