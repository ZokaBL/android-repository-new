package model;

/**
 * Created by Zoka on 31.8.2016.
 */
public class LeftMenuBean {



    private String p1;
    private String shortDecs;
    private String oper;
    private String page;
    private String imageName;
    private int image;

    public LeftMenuBean() {
    }

    public LeftMenuBean(int image) {

        this.image = image;
    }


    public LeftMenuBean(String leftMenuItem, int image, String p1, String page) {
        this.shortDecs = leftMenuItem;
        this.image = image;
        this.p1 = p1;
        this.page = page;
    }



    public String getShortDecs() {
        return shortDecs;
    }

    public void setShortDecs(String shortDecs) {
        this.shortDecs = shortDecs;
    }

    public String getOper() {
        return oper;
    }

    public void setOper(String oper) {
        this.oper = oper;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
