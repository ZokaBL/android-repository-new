package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zoka.app.R;
import java.util.ArrayList;
import fragments.HomePage;
import model.ResolveAndCheckBean;

/**
 * Created by zoka on 12.10.2016.
 */

public class AdapterHomePageButtons extends ArrayAdapter<ResolveAndCheckBean> {
    ViewHolder holder1;
    //private static final String urlMessage = "http://212.93.241.142:20282/MDBA/servlet?oper=exec&cmd=test";
    private static final String TAG = HomePage.class.getSimpleName();

    public AdapterHomePageButtons(Context context, ArrayList<ResolveAndCheckBean> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ResolveAndCheckBean resolveAllBean = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.home_page_list_row_for_buttons, parent, false);
        }
        holder1 = new ViewHolder();
        holder1.shortAlarm =(TextView)convertView.findViewById(R.id.shortAlarm);

     /*   holder1.description =(TextView)convertView.findViewById(R.id.id);
        holder1.action1 =(TextView)convertView.findViewById(R.id.action1);
        holder1.action2 =(TextView)convertView.findViewById(R.id.action2);
        holder1.action3 =(TextView)convertView.findViewById(R.id.action3);*/
        holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortAlarm.setText(resolveAllBean.getShortAlarm());
     /*   holder1.id.setText(homePageBean.getId());
        holder1.action1.setText(homePageBean.getAction1());
        holder1.action2.setText(homePageBean.getAction2());
        holder1.action3.setText(homePageBean.getAction3());*/
        //Picasso.with(getContext()).load(leviMeniBean.getImage()).into(holder1.iv);

        holder1.shortAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           /*     String id = homePageBean.getId();
                Intent alarmDetails = new Intent(getContext(), AlarmDetails.class);
                alarmDetails.putExtra("id",id);
                getContext().startActivity(alarmDetails);*/



            }
        });

        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        return convertView;
    }

    class ViewHolder {

        TextView shortAlarm;
        TextView description;
        TextView objectName;
        TextView objectOwner;
        ImageView imageView;

    }



/*    private void showMessage() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlMessage, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    HomePageAlarmsBean homePageBean = new HomePageAlarmsBean();
                    String msg = homePageBean.setMessage(response.getString("message"));
                    Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }*/

}

