package fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zoka.app.R;

/**
 * Created by zoka on 6.10.2016.
 */

public class Support extends Fragment implements View.OnClickListener{

    Button btn;
    EditText et;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support, container, false);

       // t = (TextView) view.findViewById(R.id.textView2);
        et = (EditText) view.findViewById(R.id.edittext);


  /*      if ( et.requestFocus() )
        {
            InputMethodManager imm =  (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }*/


        btn = (Button) view.findViewById(R.id.button);

        btn.setOnClickListener(this);

        final Button btn = (Button) view.findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String emailContent = et.getText().toString();
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                emailIntent.setType("vnd.android.cursor.item/email");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"dejan.maksimovic1@gmail.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Mobile DBA - problem description!");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,emailContent );
                startActivity(Intent.createChooser(emailIntent, "Send mail using..."));

            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
