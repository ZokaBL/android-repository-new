package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import activity.ExecutePageWithTabs;
import activity.ExecutePageWithoutTabs;
import com.zoka.app.R;
import model.AlarmDetailsBean;

/**
 * Created by Zoka on 23.9.2016.
 */
public class AdapterForListAlarmDetails extends ArrayAdapter<AlarmDetailsBean> {
    ViewHolder holder1;

    public AdapterForListAlarmDetails(Context context, ArrayList<AlarmDetailsBean> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final AlarmDetailsBean alarmDetailsBean = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_alarm_details, parent, false);
        }
        holder1 = new ViewHolder();
        holder1.shortDesc =(TextView)convertView.findViewById(R.id.shortDesc);
        holder1.imageView =(ImageView)convertView.findViewById(R.id.imageView2);


        convertView.setTag(holder1);
        holder1.shortDesc.setText(alarmDetailsBean.getShortDesc());


        holder1.shortDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alarmDetails = new Intent(getContext(), ExecutePageWithTabs.class);
                getContext().startActivity(alarmDetails);
                /*String id = homePageBean.getId();
                Intent alarmDetails = new Intent(getContext(), AlarmDetails.class);
                alarmDetails.putExtra("id",id);
                getContext().startActivity(alarmDetails);*/
            }
        });

        holder1.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent alarmDetails = new Intent(getContext(), ExecutePageWithoutTabs.class);
                getContext().startActivity(alarmDetails);
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView id;
        TextView action1;
        TextView action2;
        TextView action3;
        TextView shortDesc;
        ImageView imageView;

    }

}

