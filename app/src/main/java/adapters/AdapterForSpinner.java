package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.zoka.app.R;
import java.util.ArrayList;
import java.util.List;
import model.ObjectPageBean;

/**
 * Created by zoka on 4.11.2016.
 */

public class AdapterForSpinner extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<ObjectPageBean> listData;
    private Context context;
    public AdapterForSpinner(Context context, ArrayList<ObjectPageBean> listData) {
        this.context = context;
        layoutInflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = listData;
    }
    @Override
    public int getCount() {
        return listData.size();
    }
    @Override
    public Object getItem(int position) {
        return (ObjectPageBean)listData.get(position);
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder spinnerHolder;
        if(convertView == null){
            spinnerHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.spinner_list, parent, false);
            spinnerHolder.spinnerItemList = (TextView)convertView.findViewById(R.id.spinner_list_item);
            spinnerHolder.spinnerItemList.setTextSize(15);
            convertView.setTag(spinnerHolder);
        }else{
            spinnerHolder = (ViewHolder)convertView.getTag();
        }

        spinnerHolder.spinnerItemList.setText(listData.get(position).getNameDataBase());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
        textview.setText(listData.get(position).getNameDataBase());

        return textview;
    }
    class ViewHolder{
        TextView spinnerItemList;
    }
}
