package model;

/**
 * Created by zoka on 11.10.2016.
 */

public class Test {


    private String id;
    private String shortDescAlarms;
    private String action1;
    private String action2;
    private String action3;


    public Test() {
    }

    public Test(String id, String shortDescAlarms, String action1, String action2, String action3) {
        this.id = id;
        this.shortDescAlarms = shortDescAlarms;
        this.action1 = action1;
        this.action2 = action2;
        this.action3 = action3;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortDescAlarms() {
        return shortDescAlarms;
    }

    public void setShortDescAlarms(String shortDescAlarms) {
        this.shortDescAlarms = shortDescAlarms;
    }

    public String getAction1() {
        return action1;
    }

    public void setAction1(String action1) {
        this.action1 = action1;
    }

    public String getAction2() {
        return action2;
    }

    public void setAction2(String action2) {
        this.action2 = action2;
    }

    public String getAction3() {
        return action3;
    }

    public void setAction3(String action3) {
        this.action3 = action3;
    }
}
