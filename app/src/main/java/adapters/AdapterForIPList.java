package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zoka.app.R;
import java.util.List;
import model.IPBean;

public class AdapterForIPList extends ArrayAdapter<IPBean> {
    private Activity activity;
    public AdapterForIPList(Activity activity, int resource, List<IPBean> ipLista) {
        super(activity, resource, ipLista);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.single_ip, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }
        IPBean ipBean = getItem(position);
        holder.ipAdresa.setText(ipBean.getIpAddress());
      //  holder.score.setText(score.getScore());
        return convertView;
    }

    private static class ViewHolder {
        private TextView ipAdresa;
        public ViewHolder(View v) {
            ipAdresa = (TextView) v.findViewById(R.id.ip);
            //score = (TextView) v.findViewById(R.id.score);
        }
    }
}