package activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import adapters.AdapterForLeftMenu;
import adapters.CustomListAdapterLMenu;
import adapters.ViewPagerAdapter;
import com.zoka.app.R;

import appcontrol.AppController;
import model.LeftMenuBean;
import model.ListBean;
import model.Test;

import static android.R.attr.value;
import static com.android.volley.VolleyLog.TAG;

//212.93.241.142:20282

public class  MainActivity extends AppCompatActivity {

    Context context;
    String value;
    String ipFromSharedPref;

    private SharedPreferences pref;

    private static final String TAG_NAME = "body";

    // sve za left navigation drawer
    private static final String urlLeftMenu = "/MDBA/servlet?oper=menul";
    private static final String TAG = MainActivity.class.getSimpleName();
    ArrayList<LeftMenuBean> listLeviMeniItems;
    private ArrayList<Test> listLeviMeniItemsTest;
    CustomListAdapterLMenu customListAdapterLMenu;
    AdapterForLeftMenu leftAdapter;
    LeftMenuBean listItems;

    private ArrayList<ListBean> list;
    ListBean listBean;

    private ProgressDialog progressDialog;
    String message = "Pogrešna konfiguracija, ne postoji page null !";
    private ViewPager viewPager;
    private DrawerLayout drawer;
    private TabLayout tabLayout;
    private int[] icon = {
            R.mipmap.home2,
            R.mipmap.databases2,
            R.mipmap.servers2,
            R.mipmap.support,
            R.drawable.menu,

    };
    ImageView iconLeft;
    int brojIpAdresaCatch;
    String id;



    //ArrayList<LeftMenuBean> animalList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




       /* preuzimam broj IP adresa sa LoginActivity da bih prikazao poruku koja se IP adresa koristi ako ih je sacuvano
        vise od 1 */
        Intent getIP = getIntent();
        brojIpAdresaCatch = getIP.getIntExtra("numberip",0);

        pref = getSharedPreferences("ip", MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");



        if (ipFromSharedPref.equals(null)|| ipFromSharedPref.equals("")){
            Toast.makeText(getBaseContext(), "Set your first IP adresu:\n" + "Settings -> Add IP address and set", Toast.LENGTH_LONG).show();
        }

        if (brojIpAdresaCatch>1){
            LayoutInflater inflater = getLayoutInflater();
            View layouttoast = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toastcustom));
            ((TextView) layouttoast.findViewById(R.id.texttoast)).setText("Current IP address: " + ipFromSharedPref);
            Toast mytoast = new Toast(this);
            mytoast.setView(layouttoast);
            mytoast.setDuration(Toast.LENGTH_LONG);
            mytoast.show();
        }




        viewPager = (ViewPager) findViewById(R.id.view_pager);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        iconLeft = (ImageView) findViewById(R.id.imgLeftMenu);
        ListView openLeftMenuList = (ListView) findViewById(R.id.drawer_list_left);

        iconLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }

        });

        loadingLeftMenu();

        openLeftMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (listLeviMeniItems.get(position).getPage().equals("null")){
                        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
                        drawer.openDrawer(GravityCompat.START);
                    }else {
                        drawer.closeDrawer(GravityCompat.START);

                        if (listLeviMeniItems.get(position).getPage().equals("home")) {
                            viewPager.setCurrentItem(0);
                            //openUrlClickLeftMenuHome(position);
                        }else if (listLeviMeniItems.get(position).getPage().equals("tab1")) {
                            viewPager.setCurrentItem(1);
                            //openUrlClickLeftMenuHome(position);
                        }else if (listLeviMeniItems.get(position).getPage().equals("tab2")) {
                            viewPager.setCurrentItem(2);
                            //openUrlClickLeftMenuHome(position);
                        }else if (listLeviMeniItems.get(position).getPage().equals("list")) {
                            readJson(position);
                        }else if (listLeviMeniItems.get(position).getPage().equals("object")) {
                            readJson(position);
                            //finish();
                        } else{
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }
            }
        });

        //setting Tab layout (number of Tabs = number of ViewPager pages)
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        for (int i = 0; i < 5; i++) {
            tabLayout.addTab(tabLayout.newTab().setIcon(icon[i]));
        }
        //set gravity for tab bar
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //set viewpager adapter
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        //change Tab selection when swipe ViewPager
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //change ViewPager page when tab selected
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    // metoda koja procita json koji vrati url koji joj je prosledjen
    public void readJson(int position) {
        list = new ArrayList<ListBean>();
        final String urlLeftMenuHomePage = "http://" + ipFromSharedPref + "/MDBA/servlet?oper=" + listLeviMeniItems.get(position).getOper() + "&p1=";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, urlLeftMenuHomePage, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {

                    //Toast.makeText(getBaseContext(), "" + urlLeftMenuHomePage, Toast.LENGTH_SHORT).show();
                    JSONArray jArray = response.getJSONArray(TAG_NAME);
                    for (int j = 0; j <= jArray.length(); j++) {
                        JSONObject objektiNiza = jArray.getJSONObject(j);
                        listBean = new ListBean();
                        listBean.setName(objektiNiza.getString("name"));
                        list.add(listBean);

                        JSONObject jsonObject = response.getJSONObject("page");
                        value = jsonObject.getString("gotopage");

                        if (value.equals("list")) {
                            Intent intentList = new Intent(getBaseContext(), ListActivity.class);
                            intentList.putParcelableArrayListExtra("lista",  list);
                            intentList.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentList);
                            finish();
                        }else  if (value.equals("objects")) {
                            Intent intentObject = new Intent(getBaseContext(), ObjectPageActivity.class);
                            intentObject.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentObject);
                            finish();
                        } else if (value.equals("alarm_detail")) {
                            Intent intentAlarm = new Intent(getBaseContext(), AlarmDetails.class);
                            startActivity(intentAlarm);

                        } else if (value.equals("execute")) {
                            Intent intentExecute = new Intent(getBaseContext(), ExecutePage.class);
                            startActivity(intentExecute);

                        } else {
//                            Toast.makeText(context, "Nepostojeca vrednost za gotopage atribut!", Toast.LENGTH_LONG).show();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }


    // ucitavanje levog menija
    public void loadingLeftMenu() {
        listLeviMeniItems = new ArrayList<LeftMenuBean>();
        ListView listView = (ListView) findViewById(R.id.drawer_list_left);
        leftAdapter = new AdapterForLeftMenu(this, listLeviMeniItems);
        listView.setAdapter(leftAdapter);
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlLeftMenu,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                listItems = new LeftMenuBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                listItems.setP1(objectPrvi.getString("p1"));
                                listItems.setShortDecs(objectPrvi.getString("shortDecs"));
                                listItems.setOper(objectPrvi.getString("oper"));
                                listItems.setPage(objectPrvi.getString("page"));
                                listItems.setImageName(objectPrvi.getString("imageName"));

                                if (listItems.getImageName().equals("home")){
                                    listItems.setImage(R.drawable.home);
                                }else if (listItems.getImageName().equals("relations")){
                                    listItems.setImage(R.drawable.relations);
                                }
                                else if (listItems.getImageName().equals("invalid")){
                                    listItems.setImage(R.drawable.invalid);
                                }
                                else if (listItems.getImageName().equals("servers2")){
                                    listItems.setImage(R.drawable.servers2);
                                }
                                else if (listItems.getImageName().equals("schedulerjobs")){
                                    listItems.setImage(R.drawable.schedulerjobs);
                                }
                                else if (listItems.getImageName().equals("jobs")){
                                    listItems.setImage(R.drawable.jobs);
                                }else if (listItems.getImageName().equals("users")){
                                    listItems.setImage(R.drawable.users);
                                }
                                else if (listItems.getImageName().equals("roles")){
                                    listItems.setImage(R.drawable.roles);
                                }
                                else if (listItems.getImageName().equals("triggers")){
                                    listItems.setImage(R.drawable.triggers);
                                }
                                else if (listItems.getImageName().equals("sequences")){
                                    listItems.setImage(R.drawable.sequences);
                                }
                                else if (listItems.getImageName().equals("tables")){
                                    listItems.setImage(R.drawable.tables);
                                }
                                else if (listItems.getImageName().equals("views")){
                                    listItems.setImage(R.drawable.views);
                                }
                                else if (listItems.getImageName().equals("dblinks")){
                                    listItems.setImage(R.drawable.dblinks);
                                }
                                else if (listItems.getImageName().equals("tablespaces")){
                                    listItems.setImage(R.drawable.tablespaces);
                                }
                                else if (listItems.getImageName().equals("favourites")){
                                    listItems.setImage(R.drawable.favourites);
                                }
                                else if (listItems.getImageName().equals("indexes2")){
                                    listItems.setImage(R.drawable.indexes2);
                                }
                                else if (listItems.getImageName().equals("indexes")){
                                    listItems.setImage(R.drawable.indexes);
                                }
                                else if (listItems.getImageName().equals("home2")){
                                    listItems.setImage(R.drawable.home2);
                                }
                                else if (listItems.getImageName().equals("databases")){
                                    listItems.setImage(R.drawable.databases);
                                }
                                else if (listItems.getImageName().equals("settings")){
                                    listItems.setImage(R.drawable.settings);
                                }
                                else if (listItems.getImageName().equals("help")){
                                    listItems.setImage(R.drawable.help);
                                }
                                else if (listItems.getImageName().equals("lists")){
                                    listItems.setImage(R.drawable.lists);
                                }
                                else if (listItems.getImageName().equals("settings2")){
                                    listItems.setImage(R.drawable.settings2);
                                }
                                else if (listItems.getImageName().equals("packages")){
                                    listItems.setImage(R.drawable.packages);
                                }
                                else if (listItems.getImageName().equals("servers")){
                                    listItems.setImage(R.drawable.servers);
                                }
                                else if (listItems.getImageName().equals("settings3")){
                                    listItems.setImage(R.drawable.settings3);
                                }
                                else if (listItems.getImageName().equals("documents")){
                                    listItems.setImage(R.drawable.documents);
                                }
                                // dodati sliku objects
                                else if (listItems.getImageName().equals("objects")){
                                    listItems.setImage(R.drawable.invalid);
                                }else {
                                    listItems.setImage(R.mipmap.ic_launcher);
                                }
                                //listItems.setImage(R.drawable.icon_servers);
                                listLeviMeniItems.add(listItems);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        leftAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }

    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void showDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(this.getResources().getString(R.string.load));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    @Override
    public void onBackPressed() {
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();

        }
    }
}

