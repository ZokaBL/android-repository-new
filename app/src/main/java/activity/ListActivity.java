package activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.zoka.app.R;

import java.util.ArrayList;
import java.util.List;

import adapters.AdapterForListActivity;
import model.ListBean;

/**
 * Created by zoka on 23.1.2017.
 */

public class ListActivity extends AppCompatActivity {

    AdapterForListActivity adapterForListActivity;

    ListView listView;
    List<ListBean> li;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.list_activity);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.list_activity);
        Intent intent  = getIntent();
        li = (List) intent.getParcelableArrayListExtra("lista");
        adapterForListActivity = new AdapterForListActivity(this, li);
        listView.setAdapter(adapterForListActivity);

    }

    @Override
    public void onBackPressed() {
        Intent objectPage = new Intent(ListActivity.this, MainActivity.class);
        objectPage.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(objectPage);
        finish();
    }

}
