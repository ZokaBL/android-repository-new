package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zoka.app.R;

import java.util.ArrayList;
import java.util.List;

import model.Database;

/**
 * Created by zoka on 8.4.2017.
 */

public class AdapterForSpinerStatus extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<String> listData;
    private Context context;
    public AdapterForSpinerStatus(Context context, ArrayList<String> listData) {
        this.context = context;
        layoutInflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = listData;
    }
    @Override
    public int getCount() {
        return listData.size();
    }
    @Override
    public Object getItem(int position) {
        return (String)listData.get(position);
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AdapterForSpinerStatus.ViewHolder spinnerHolder;
        if(convertView == null){
            spinnerHolder = new AdapterForSpinerStatus.ViewHolder();
            convertView = layoutInflater.inflate(R.layout.spinner_list, parent, false);
            spinnerHolder.spinnerItemList = (TextView)convertView.findViewById(R.id.spinner_list_item);
            spinnerHolder.spinnerItemList.setTextSize(15);
            convertView.setTag(spinnerHolder);
        }else{
            spinnerHolder = (AdapterForSpinerStatus.ViewHolder)convertView.getTag();
        }

        spinnerHolder.spinnerItemList.setText(listData.get(position));
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
        textview.setText(listData.get(position));
        //textview.setTextColor(Color.parseColor("#46629E"));
        textview.setPadding(8, 8, 8, 8);
        return textview;
    }
    class ViewHolder{
        TextView spinnerItemList;

    }
}