package activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.zoka.app.R;;


/**
 * Created by zoka on 28.9.2016.
 */

public class ExecutePageWithoutTabs extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.execute_page_without_tabs);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.execute_activity);
        setSupportActionBar(toolbar);


    }

}
