package fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import activity.DatabasePage;
import activity.ObjectPageActivity;
import adapters.AdapterForLeftMenu;
import adapters.AdapterForListDatabasesTab;
import model.Database;
import model.LeftMenuBean;
import model.ListBean;



public class Databases extends Fragment {

    private ProgressDialog progressDialog;
    ImageView buttonAddDatabase;

    private static final String TAG = Databases.class.getSimpleName();
    private static final String TAG_NAME = "body";

    private SharedPreferences pref;
    String ipFromSharedPref;

    private static final String urlDatabases = "/MDBA/servlet?oper=databases&p1=";

    AdapterForListDatabasesTab customListAdapterDatabase;
    private ArrayList<Database> listBeanItems;

    //ArrayList<String> typeDataBases;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_database, container, false);
        pref = getActivity().getSharedPreferences("ip", getContext().MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip", "");


        buttonAddDatabase = (ImageView) rootView.findViewById(R.id.buttonAdd);

        buttonAddDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addDatabase = new Intent(getContext(), DatabasePage.class);
                startActivity(addDatabase);
            }
        });

        listBeanItems = new ArrayList<Database>();
        //typeDataBases = new ArrayList<String>();

        ListView listView = (ListView) rootView.findViewById(R.id.database_list);

        customListAdapterDatabase = new AdapterForListDatabasesTab(getContext(), listBeanItems);
        listView.setAdapter(customListAdapterDatabase);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlDatabases,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                Database database = new Database();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                database.setId(objectPrvi.getInt("id"));
                                database.setName(objectPrvi.getString("name"));
                                database.setHostname(objectPrvi.getString("hostname"));
                                database.setPort(objectPrvi.getInt("port"));
                                database.setDatabase(objectPrvi.getString("database"));
                                database.setStatus(objectPrvi.getString("status"));
                                database.setDbType(objectPrvi.getInt("dbType"));

                           /*     JSONObject objectServer = objectPrvi.getJSONObject("server");
                                database.setServer(objectServer.getString("ip"));*/

                                listBeanItems.add(database);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        customListAdapterDatabase.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });

        queue.add(jsonArrayRequest);
        return rootView;
    }


    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        hidePDialog();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
