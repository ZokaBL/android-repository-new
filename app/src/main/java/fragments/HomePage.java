package fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import adapters.AdapterHomePageButtons;
import adapters.AdapterForListAlarmsHomePage;
import appcontrol.AppController;
import com.zoka.app.R;
import model.HomePageAlarmsBean;
import model.ResolveAndCheckBean;



public class HomePage extends Fragment {

    private SharedPreferences pref;

    private static final String TAG = HomePage.class.getSimpleName();
    private ArrayList<HomePageAlarmsBean> listMeniItems;
    private ArrayList<ResolveAndCheckBean> listResolve;

    AdapterForListAlarmsHomePage customListAdapterHomePage;
    AdapterHomePageButtons adapterButons;

    private ProgressDialog progressDialog;
    private static final String TAG_NAME = "list";
    private static final String urlHomePage = "/MDBA/servlet?oper=alarms";
    private static final String urlResolveAll = "/MDBA/servlet?oper=resall";
    private static final String urlCheckAll = "/MDBA/servlet?oper=chkall";
    private static final String urlMessage = "/MDBA/servlet?oper=exec&cmd=execute_page";

    String checkAllValue;
    ResolveAndCheckBean resolveList;
    String urlUhvacen;
    String ipFromSharedPref;

    public HomePage() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.home_page, container, false);


        pref = getActivity().getSharedPreferences("ip", getContext().MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");

        Intent getid = getActivity().getIntent();
        urlUhvacen = getid.getStringExtra("url_from_object_page");

        if (urlUhvacen==null){

        listMeniItems = new ArrayList<HomePageAlarmsBean>();
        ListView listView = (ListView) rootView.findViewById(R.id.list_view_fragment_one);

        customListAdapterHomePage = new AdapterForListAlarmsHomePage(getContext(), listMeniItems);
        listView.setAdapter(customListAdapterHomePage);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        showPDialog();
        final JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, "http://" + ipFromSharedPref + urlHomePage, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                try {

                    JSONArray jArray = response.getJSONArray(TAG_NAME);
                    for (int i=0; i<=response.length();i++) {
                        for (int j=0; j<=jArray.length();j++) {
                            HomePageAlarmsBean homePageBean = new HomePageAlarmsBean();
                            JSONObject obj = response.getJSONObject("header");
                            String buttonText = homePageBean.setButton1(obj.getString("button1"));
                            String text1 = homePageBean.setText1(obj.getString("text1"));
                            checkAllValue = homePageBean.setCheckAll(obj.getString("action1"));

                            Button btn = (Button) rootView.findViewById(R.id.btncheckall);

                            if (buttonText.equals(null)|| buttonText.equals("")){
                                btn.setVisibility(View.INVISIBLE);

                            }else {
                                btn.setVisibility(View.VISIBLE);
                                btn.setText(buttonText);
                            }

                            TextView textView = (TextView) rootView.findViewById(R.id.text1);
                            textView.setText(text1);

                            JSONObject object2 = jArray.getJSONObject(j);
                            homePageBean.setShortDescAlarms(object2.getString("shortDesc"));
                            homePageBean.setId(object2.getString("id"));
                            homePageBean.setAction1(object2.getString("action1"));
                            homePageBean.setAction2(object2.getString("action2"));
                            homePageBean.setAction3(object2.getString("action3"));
                            listMeniItems.add(homePageBean);


                        }
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                customListAdapterHomePage.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        queue.add(jsonArrayRequest);

        }else {

            listMeniItems = new ArrayList<HomePageAlarmsBean>();
            ListView listView = (ListView) rootView.findViewById(R.id.list_view_fragment_one);


            customListAdapterHomePage = new AdapterForListAlarmsHomePage(getContext(), listMeniItems);
            listView.setAdapter(customListAdapterHomePage);


            RequestQueue queue = Volley.newRequestQueue(getActivity());
            showPDialog();
            final JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, urlUhvacen, null, new Response.Listener<JSONObject>(){
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response.toString());
                    hidePDialog();
                    try {

                        JSONArray jArray = response.getJSONArray(TAG_NAME);
                        for (int i=0; i<=response.length();i++) {
                            for (int j=0; j<=jArray.length();j++) {
                                HomePageAlarmsBean homePageBean = new HomePageAlarmsBean();
                                JSONObject obj = response.getJSONObject("header");
                                String buttonText = homePageBean.setButton1(obj.getString("button1"));
                                String text1 = homePageBean.setText1(obj.getString("text1"));
                                checkAllValue = homePageBean.setCheckAll(obj.getString("action1"));

                                Button btn = (Button) rootView.findViewById(R.id.btncheckall);
                                btn.setText(buttonText);

                                TextView textView = (TextView) rootView.findViewById(R.id.text1);
                                textView.setText(text1);

                                JSONObject object2 = jArray.getJSONObject(j);
                                homePageBean.setShortDescAlarms(object2.getString("shortDesc"));
                                homePageBean.setId(object2.getString("id"));
                                homePageBean.setAction1(object2.getString("action1"));
                                homePageBean.setAction2(object2.getString("action2"));
                                homePageBean.setAction3(object2.getString("action3"));
                                listMeniItems.add(homePageBean);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    customListAdapterHomePage.notifyDataSetChanged();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    hidePDialog();
                }
            });
            queue.add(jsonArrayRequest);
        }
        return rootView;
    }


    private void showMessage() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://"+ ipFromSharedPref + urlMessage, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    HomePageAlarmsBean homePageBean = new HomePageAlarmsBean();
                    String msg = homePageBean.setMessage(response.getString("message"));
                    Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    // metoda koja se poziva klikom na dugme CHECK ALL
    private void checkAll(View rootView){
        listResolve = new ArrayList<ResolveAndCheckBean>();
        ListView listView = (ListView) rootView.findViewById(R.id.list_view_fragment_one);
        adapterButons = new AdapterHomePageButtons(getContext(), listResolve);
        listView.setAdapter(adapterButons);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        showPDialogRefresh();
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlCheckAll,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();
                        showMessage();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                resolveList = new ResolveAndCheckBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                resolveList.setShortAlarm(objectPrvi.getString("shortAlarm"));

                                listResolve.add(resolveList);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapterButons.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
            }
        });
        queue.add(jsonArrayRequest);
    }


    // metoda koja se poziva klikom na dugme RESOLVE ALL
    private void resolveAll(View rootView){
        listResolve = new ArrayList<ResolveAndCheckBean>();
        ListView listView = (ListView) rootView.findViewById(R.id.list_view_fragment_one);
        adapterButons = new AdapterHomePageButtons(getContext(), listResolve);
        listView.setAdapter(adapterButons);
        RequestQueue queue = Volley.newRequestQueue(getContext());
        showPDialog();
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + urlResolveAll,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                resolveList = new ResolveAndCheckBean();
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                resolveList.setShortAlarm(objectPrvi.getString("shortAlarm"));
                                listResolve.add(resolveList);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapterButons.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });
        queue.add(jsonArrayRequest);

    }


    public void showPDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getActivity().getResources().getString(R.string.load));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    public void showPDialogRefresh() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(getActivity().getResources().getString(R.string.refresh));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    private void hidePDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        hidePDialog();
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       Button checkAll = (Button) view.findViewById(R.id.btncheckall);
        Button resolveAll = (Button) view.findViewById(R.id.btnresolveall);
        checkAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAll(view);
                //showMessage();
            }
        });

        resolveAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resolveAll(view);

            }
        });

    }

}
