package activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import adapters.AdapterForSpinerStatus;
import adapters.AdapterForSpinerTypeDatabase;
import model.DatabasesTypeList;

/**
 * Created by zoka on 8.4.2017.
 */

public class ServersPage extends AppCompatActivity {

    private static final String TAG = ServersPage.class.getSimpleName();
    String catchIp;
    String catchName;
    String catchUsername;
    String catchPassword;
    String catchPort;

    private EditText ip;
    private EditText name;
    private EditText username;
    private EditText password;
    private EditText port;
    private Spinner spinStatus;
    private Button cancel;
    private Button save;

    AdapterForSpinerStatus adapterStatus;

    private ArrayList<String> dropDownListStatus;

    private SharedPreferences pref;
    String ipFromSharedPref;

    private String idSelectedStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_server_page_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.add_server);
        setSupportActionBar(toolbar);

        pref = this.getSharedPreferences("ip",MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip", "");

        loadingDropDownStatus();


        Intent getid = getIntent();
        catchIp = getid.getStringExtra("ip");
        catchName = getid.getStringExtra("name");
        catchUsername = getid.getStringExtra("username");
        catchPassword = getid.getStringExtra("password");
        catchPort = getid.getStringExtra("port");

        ip = (EditText) findViewById(R.id.edittextIp);
        ip.setText(catchIp);


        name = (EditText) findViewById(R.id.edittextName);
        name.setText(catchName);

        username = (EditText) findViewById(R.id.edittextUsername);
        username.setText(catchUsername);

        password = (EditText) findViewById(R.id.edittextPasword);
        password.setText(catchPassword);

         port = (EditText) findViewById(R.id.edittextPort);
        port.setText(catchPort);


        cancel = (Button) findViewById(R.id.buttonCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        save = (Button) findViewById(R.id.buttonSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), idSelectedStatus, Toast.LENGTH_LONG).show();

            }
        });

    }

    public void loadingDropDownStatus() {
        dropDownListStatus = new ArrayList<String>();
        dropDownListStatus.add("A");
        dropDownListStatus.add("N");
        spinStatus = (Spinner) findViewById(R.id.spinnerStatus);
        adapterStatus = new AdapterForSpinerStatus(getBaseContext(), dropDownListStatus);
        spinStatus.setAdapter(adapterStatus);
        spinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idSelectedStatus = dropDownListStatus.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
