package util;

import android.content.Context;
import android.content.SharedPreferences;

public class MySharedPreference {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    // Context
    private Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "pref";
    private static final String REZULTAT = "rezultat";

    public MySharedPreference(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void saveList(String rezString) {
        editor.putString(REZULTAT, rezString);
        editor.commit();
    }

    public String getList() {

        return pref.getString(REZULTAT, "");
    }



}
