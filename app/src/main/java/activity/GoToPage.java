package activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import appcontrol.AppController;
import model.LeftMenuBean;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by zoka on 18.1.2017.
 */

public class GoToPage {

    Context context;
    String value;

    public GoToPage(Context context){
        this.context = context;
    }

    public void readJson(String url) {

        final JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    JSONObject objectPage = response.getJSONObject("page");
                    value = objectPage.getString("gotopage");

                    if (value.equals("object")){
                        Intent intentObject = new Intent(context,ObjectPageActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentObject);
                        //intentObject.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    }else if(value.equals("home")) {
                        Intent intentHome = new Intent(context,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentHome);

                    }else if(value.equals("list")) {
                        Intent intentList = new Intent(context,ListActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentList);
                    }else{
                        Toast.makeText(context, "Nepostojeca vrednost za gotopage atribut!", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

}

