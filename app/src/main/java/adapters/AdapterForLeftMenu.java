package adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import com.zoka.app.R;

import activity.AlarmDetails;
import activity.GoToPage;
import activity.MainActivity;
import model.LeftMenuBean;

/**
 * Created by Zoka on 1.9.2016.
 */
public class AdapterForLeftMenu extends ArrayAdapter<LeftMenuBean>{
    //ViewHolder holder1;

    ArrayList<LeftMenuBean> lista = new ArrayList<>();

    public AdapterForLeftMenu(Context context, ArrayList<LeftMenuBean> items) {
        super(context, 0, items);
        lista = items;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final LeftMenuBean leviMeniBean = getItem(position);
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.single_data_left_menu, null);
        TextView textView = (TextView) view.findViewById(R.id.shortDecs);
        ImageView imageView = (ImageView) view.findViewById(R.id.imgView);
        textView.setText(leviMeniBean.getShortDecs());
        imageView.setImageResource(leviMeniBean.getImage());

        return view;
    }





}

