package model;

/**
 * Created by Zoka on 5.9.2016.
 */
public class HomePageAlarmsBean {


    // alarms
    private String id;
    private String shortDescAlarms;
    private String action1;
    private String action2;
    private String action3;

    // header
    private String text1;
    private String button1;
    private String checkAll;


    // message
    private String message;




    public HomePageAlarmsBean() {
    }

    public HomePageAlarmsBean(String id, String shortDescAlarms, String action1, String action2, String action3, String text1, String button1, String checkAll, String message, String shortAlarm, String description, String objectName, String objectOwner) {
        this.id = id;
        this.shortDescAlarms = shortDescAlarms;
        this.action1 = action1;
        this.action2 = action2;
        this.action3 = action3;
        this.text1 = text1;
        this.button1 = button1;
        this.checkAll = checkAll;
        this.message = message;

    }

    public String getAction1() {
        return action1;
    }

    public String setAction1(String action1) {
        this.action1 = action1;
        return action1;
    }


    // shortDesc
    public String getShortDescAlarms()
    {

        return shortDescAlarms;
    }

    public void setShortDescAlarms(String shortDesc)
    {
        this.shortDescAlarms = shortDesc;
    }

    //id

    public String getId() {

        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getAction2() {
        return action2;
    }

    public void setAction2(String action2) {
        this.action2 = action2;
    }

    public String getAction3() {
        return action3;
    }

    public void setAction3(String action3) {
        this.action3 = action3;
    }

    public String getText1() {
        return text1;
    }

    public String setText1(String text1) {
        this.text1 = text1;
        return text1;
    }

    public String getButton1() {
        return button1;
    }

    public String setButton1(String button1) {
        this.button1 = button1;
        return button1;
    }

    public String getCheckAll() {
        return checkAll;
    }

    public String setCheckAll(String checkAll) {
        this.checkAll = checkAll;
        return checkAll;
    }

    public String getMessage() {
        return message;
    }

    public String setMessage(String message) {
        this.message = message;
        return message;
    }

}
