package model;

/**
 * Created by zoka on 11.4.2017.
 */

public class DatabasesTypeList {

    private int id;
    private String name;

    public DatabasesTypeList() {
    }

    public DatabasesTypeList(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
