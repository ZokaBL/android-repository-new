package adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import fragments.Settings;
import fragments.Servers;
import fragments.Databases;
import fragments.HomePage;
import fragments.Support;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {

            return new HomePage();

        } else if (position == 1) {

            return new Databases();

        } else if (position == 2) {

            return new Servers();

        } else if (position == 3) {

            return new Support();
        }
        else return new Settings();
    }


    @Override
    public int getCount() {

        return 5;
    }
}
