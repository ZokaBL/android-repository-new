package activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zoka.app.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import adapters.AdapterForIPList;
import model.IPBean;
import util.MySharedPreference;


public class SettingsActivity extends AppCompatActivity {

    private View btnAdd;
    private View btnGet;
    private EditText textName;
    private ViewGroup buttonLayout, inputLayout, listViewLayout;
    private ListView ipAddressList;
    private View btnOK;
    private View btnCancel;



    private ArrayList<IPBean> ipLista;
    private MySharedPreference sharedPreference;

    private Gson gson;
    private SharedPreferences pref;
    private String prefIP = "ip";

    AdapterForIPList adapterForIPList;

    TextView pozicija;
    EditText editText11;

    int pozicijaListStavke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);


  
        //scores = new ArrayList<IPBean>();
        gson = new Gson();
        sharedPreference = new MySharedPreference(getApplicationContext());

        // metoda koja prikazuje listu IP adresa
        showIpList();

        // metoda koja prikazuje layout
        findView();

        //set event for buttons
        btnAdd.setOnClickListener(onAddListener());
        btnGet.setOnClickListener(onGettingDataListener());
        btnOK.setOnClickListener(onConfirmListener());
        btnCancel.setOnClickListener(onCancelListener());

        ListView listv = (ListView) findViewById(R.id.list);
        listv.setAdapter(adapterForIPList);
        registerForContextMenu(listv);

        ipAddressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String izabraniIP = ipLista.get(position).getIpAddress();
                pref = getSharedPreferences(prefIP, MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("ip", izabraniIP);
                editor.commit();
                Intent i = new Intent(getBaseContext(),MainActivity.class);
                startActivity(i);
                finish();

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        pozicijaListStavke = info.position;
        menu.setHeaderTitle("IP: " + ipLista.get(info.position).getIpAddress());
        String[] menuItems = getResources().getStringArray(R.array.context_menu);
        for (int i = 0; i<menuItems.length; i++) {
            menu.add(Menu.NONE, i, i, menuItems[i]);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if(item.getTitle().equals("Edit")) {
            editIP(ipLista.get(info.position),pozicijaListStavke);
        }

       else if(item.getTitle().equals("Delete")) {
            deleteIP(ipLista.remove(info.position));
        }
            return true;
    }

    private void findView() {
        ipAddressList = (ListView)findViewById(R.id.list);
        buttonLayout = (ViewGroup)findViewById(R.id.layout_add);
        inputLayout = (ViewGroup)findViewById(R.id.layout_input);
        btnAdd = findViewById(R.id.btn_add);
        btnGet = findViewById(R.id.btn_get);
        btnOK = findViewById(R.id.btn_ok);
        btnCancel = findViewById(R.id.btn_cancel);
        textName = (EditText)findViewById(R.id.txt_name);
        listViewLayout = (ViewGroup)findViewById(R.id.layout_listview);
    }

    private void saveToSharedpreference(ArrayList<IPBean> list) {
        //convert ArrayList object to String by Gson
        String jsonScore = gson.toJson(list);
        //save to shared preference
        sharedPreference.saveList(jsonScore);

    }


    // metoda vraca podatke iz sharepreference

    private void showIpList() {
        //retrieve data from shared preference
        String jsonScore = sharedPreference.getList();
        Type type = new TypeToken<List<IPBean>>(){}.getType();
        ipLista = gson.fromJson(jsonScore, type);
        if (ipLista == null) {
            ipLista = new ArrayList<>();
        }
    }

    private View.OnClickListener onAddListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listViewLayout.setVisibility(View.GONE);
                inputLayout.setVisibility(View.VISIBLE);

            }
        };
    }

    private View.OnClickListener onGettingDataListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ipLista.size() == 0) {
                    Toast.makeText(SettingsActivity.this, "Nema sacuvanih IP adresa", Toast.LENGTH_SHORT).show();
                } else {
                    showIpList(); //get data
                    //set adapter for listview and visible it
                    AdapterForIPList adapter = new AdapterForIPList(SettingsActivity.this, R.layout.single_ip, ipLista);
                    ipAddressList.setAdapter(adapter);
                    listViewLayout.setVisibility(View.VISIBLE);
                    inputLayout.setVisibility(View.GONE);
                }
            }
        };
    }


    // button add
    private View.OnClickListener onConfirmListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textName.getText().toString().equals("")) {
                    //not null input
                    IPBean ipBean = new IPBean();;
                    ipBean.setIpAddress(textName.getText().toString());
                    ipLista.add(ipBean); //add to scores list
                    saveToSharedpreference(ipLista); //save to share pref
                    //clear edit text data
                    textName.setText("");
                    Toast toast= Toast.makeText(getApplicationContext(),
                            "Set IP from address list", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER| Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();
                    /*startActivity(getIntent());
                    showIpList();*/
                    inputLayout.setVisibility(View.GONE);
                } else {
                    Log.e("Activity", "null input");
                }

            }

        };
    }


    private void deleteIP(IPBean positionItem){
        ipLista.remove(positionItem);
        saveToSharedpreference(ipLista);
        startActivity(getIntent());
        finish();

    }
    

    private void editIP(IPBean ipName, int position){
        String nameIP = ipName.getIpAddress();
        editIpAdressDialogBox(nameIP,position);
    }



    private View.OnClickListener onCancelListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gone input layout
                inputLayout.setVisibility(View.GONE);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



    public void editIpAdressDialogBox(final String ipName,final int pozicija){
        editText11 = new EditText(SettingsActivity.this);
        editText11.setText(ipName);
        final AlertDialog dialog = new AlertDialog.Builder(SettingsActivity.this)
                .setMessage("Edit your IP:")
                .setView(editText11)
                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String newIP = editText11.getText().toString();
                        IPBean ipBean2 = new IPBean();
                        ipBean2.setIpAddress(newIP);
                        ipLista.set(pozicija,ipBean2);
                        saveToSharedpreference(ipLista);
                        startActivity(getIntent());
                        finish();

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .create();
        editText11.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });

        dialog.show();

    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(),MainActivity.class);
        startActivity(i);
        finish();
    }

}