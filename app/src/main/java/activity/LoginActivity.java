package activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.zoka.app.R;
import util.MySharedPreference;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private static final String korisnik = "admin";
    private static final String pass = "admin";
    ProgressDialog progressDialog;
    EditText userNameText;
    EditText passwordText;
    Button loginButton;
    CheckBox checkBox;
    private SharedPreferences prefs;
    private String prefName = "report";
    private MySharedPreference sharedPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        prefs = getSharedPreferences(prefName, MODE_PRIVATE);
        boolean stateCheckBox = prefs.getBoolean("state",false);

        loginButton = (Button) findViewById(R.id.btn_login);
        checkBox = (CheckBox) findViewById(R.id.checkBox);

            userNameText = (EditText) findViewById(R.id.username);
            userNameText.setText(prefs.getString("user", ""));
            passwordText = (EditText) findViewById(R.id.password);
            passwordText.setText(prefs.getString("password", ""));


        if (stateCheckBox == true) {
            checkBox.setChecked(true);
        }else {
            checkBox.setChecked(false);
        }
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkBox.isChecked()) {
                        prefs = getSharedPreferences(prefName, MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user", userNameText.getText().toString());
                        editor.putString("password", passwordText.getText().toString());
                        editor.putBoolean("state", true);
                        //saves values
                        if (userNameText.getText().toString().equals(korisnik) && passwordText.getText().toString().equals(pass)){
                            editor.commit();
                        }else {

                        }
                    }

                    if (!checkBox.isChecked()){
                        prefs = getSharedPreferences(prefName, MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user", "");
                        editor.putString("password", "");
                        editor.putBoolean("state", false);
                        //saves values
                        editor.commit();
                    }
                }

            });

            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userNameText.getText().length()==0 || passwordText.getText().length()==0){
                        userNameText.setText("");
                        passwordText.setText("");
                        checkBox.setChecked(false);
                    }
                    if ((!userNameText.getText().toString().equals(korisnik)) || (!passwordText.getText().toString().equals(pass))){
                        userNameText.setText("");
                        passwordText.setText("");
                        userNameText.requestFocus();
                        checkBox.setChecked(false);
                    }
                    final ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    NetworkInfo ninfo = cm.getActiveNetworkInfo();
                    if (ninfo != null && ninfo.isConnected()) {
                        login();

                    }else {
                        alertMessage();
                    }
                }
            });
    }

    public void login() {
        Log.d(TAG, "Login");
        if (!validate()) {
            onLoginFailed();
            return;
        }
            loginButton.setEnabled(false);
            showVerificationDialog();
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onLoginSuccess();
                            progressDialog.dismiss();
                        }
                    }, 3000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        /*klikom na button login saljem na Main Activity broj IP adresa koje su sacuvane */
        sharedPreference = new MySharedPreference(getApplicationContext());
        String listIpAddress = sharedPreference.getList();
        String[] array = listIpAddress.split(",");
        int brojIpAdresa = array.length;
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        i.putExtra("numberip",brojIpAdresa);
        startActivity(i);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Incorrect username or password, try again!", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;
        String user = userNameText.getText().toString();
        String password = passwordText.getText().toString();
        if (user.isEmpty() || user.length() < 4 || !user.equals(korisnik)  ) {
            valid = false;
        } else {
            userNameText.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || !password.equals(pass)) {
            valid = false;
        } else {
            passwordText.setError(null);
        }
        return valid;
    }

    public void showVerificationDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(this.getResources().getString(R.string.load_verification));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    public void alertMessage() {
        final ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(Html.fromHtml("<font color='#E43E3E'>Warning!</font>"))
                .setMessage(R.string.check_conn)
                .setCancelable(false)
                .setIcon(R.drawable.attention)
                .setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @TargetApi(11)
                            public void onClick(DialogInterface dialog, int id) {
                                NetworkInfo ninfo2 = cm.getActiveNetworkInfo();
                                if (ninfo2 == null) {
                                    dialog.dismiss();
                                    builder.show();
                                }else {
                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @TargetApi(11)
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                }).show();
    }
}
