package activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import adapters.AdapterForSpinerStatus;
import adapters.AdapterForSpinerTypeDatabase;
import model.DatabasesTypeList;

/**
 * Created by zoka on 5.4.2017.
 */

public class DatabasePage extends AppCompatActivity {

    private static final String TAG = DatabasePage.class.getSimpleName();

    String catchName;
    String catchHostName;
    String catchPort;
    String catchDatabase;
    int catchDbType;


    private EditText name;
    private EditText hostname;
    private EditText port;
    private EditText database;
    private Spinner spinTypeData;
    private Spinner spinStatus;
    private Button cancel;
    private Button save;
    private Button delete;

    private ArrayList<DatabasesTypeList> dropDownListDatabasesType;
    private ArrayList<String> dropDownListStatus;
    DatabasesTypeList databasesTypeList;

    AdapterForSpinerTypeDatabase adapterTypeDatabase;
    AdapterForSpinerStatus adapterStatus;


    private static final String URL_DATABASES_TYPE_LIST = "/MDBA/servlet?oper=databasetypelist";



    private SharedPreferences pref;
    String ipFromSharedPref;



    String idSelectedDatabasesType;
    String idSelectedStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_database_page_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.add_database);
        setSupportActionBar(toolbar);

        pref = this.getSharedPreferences("ip",MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip", "");


        loadingDropDownTypeDatabases();

        loadingDropDownStatus();


        Intent getid = getIntent();
        catchDbType = getid.getIntExtra("dbtype",0);

        catchName = getid.getStringExtra("name");
        catchHostName = getid.getStringExtra("hostname");
        catchPort = getid.getStringExtra("port");
        catchDatabase = getid.getStringExtra("database");

        name = (EditText) findViewById(R.id.edittextName);
        name.setText(catchName);


        hostname = (EditText) findViewById(R.id.edittextHostName);
        hostname.setText(catchHostName);

        port = (EditText) findViewById(R.id.edittextPort);
        port.setText(catchPort);

        database = (EditText) findViewById(R.id.edittextDatabase);
        database.setText(catchDatabase);

        cancel = (Button) findViewById(R.id.buttonCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        save = (Button) findViewById(R.id.buttonSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        delete = (Button) findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogForDelete();
            }
        });

    }




    public void loadingDropDownTypeDatabases() {
        dropDownListDatabasesType = new ArrayList<DatabasesTypeList>();
        RequestQueue queue = Volley.newRequestQueue(getBaseContext());
        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://" + ipFromSharedPref + URL_DATABASES_TYPE_LIST,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject objectPrvi = (JSONObject) response.get(i);
                                databasesTypeList = new DatabasesTypeList();
                                databasesTypeList.setId(objectPrvi.getInt("id"));
                                databasesTypeList.setName(objectPrvi.getString("name"));
                                dropDownListDatabasesType.add(databasesTypeList);

                            }


                            if (catchDbType!=0) {
                                Collections.swap(dropDownListDatabasesType, 0, catchDbType - 1);
                                spinTypeData = (Spinner) findViewById(R.id.spinnerDatabaseType);
                                adapterTypeDatabase = new AdapterForSpinerTypeDatabase(getBaseContext(), dropDownListDatabasesType);
                                spinTypeData.setAdapter(adapterTypeDatabase);

                            }
                            spinTypeData = (Spinner) findViewById(R.id.spinnerDatabaseType);
                            adapterTypeDatabase = new AdapterForSpinerTypeDatabase(getBaseContext(), dropDownListDatabasesType);
                            spinTypeData.setAdapter(adapterTypeDatabase);

                            spinTypeData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    idSelectedDatabasesType = dropDownListDatabasesType.get(position).getName();
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }



    public void loadingDropDownStatus() {
        dropDownListStatus = new ArrayList<String>();
        dropDownListStatus.add("A");
        dropDownListStatus.add("N");
        spinStatus = (Spinner) findViewById(R.id.spinnerStatus);
        adapterStatus = new AdapterForSpinerStatus(getBaseContext(), dropDownListStatus);
        spinStatus.setAdapter(adapterStatus);
        spinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                       idSelectedStatus = dropDownListStatus.get(position);
                                    }
                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

    }


    private void createDialogForDelete() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.dialog_obavjestenje);
        alert.setMessage(R.string.dialog_delete);
        alert.setIcon(R.drawable.attention);
        alert.setCancelable(false);
        alert.setPositiveButton("DA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getApplicationContext(), "Saljem ID", Toast.LENGTH_SHORT).show();

            }
        });

        alert.setNegativeButton("NE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });
        alert.create().show();
    }

}
