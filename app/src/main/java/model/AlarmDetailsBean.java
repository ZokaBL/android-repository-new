package model;

/**
 * Created by Zoka on 23.9.2016.
 */
public class AlarmDetailsBean {

    private String hint;
    private String shortDesc;
    private String id;
    private String action1;
    private String action2;
    private String button1;
    private String action3;

    public AlarmDetailsBean() {
    }

    public AlarmDetailsBean(String action1, String action2, String action3, String button1, String hint, String id, String shortDesc) {
        this.action1 = action1;
        this.action2 = action2;
        this.action3 = action3;
        this.button1 = button1;
        this.hint = hint;
        this.id = id;
        this.shortDesc = shortDesc;
    }

    public String getAction1() {
        return action1;
    }

    public void setAction1(String action1) {
        this.action1 = action1;
    }

    public String getAction2() {
        return action2;
    }

    public void setAction2(String action2) {
        this.action2 = action2;
    }

    public String getAction3() {
        return action3;
    }

    public void setAction3(String action3) {
        this.action3 = action3;
    }

    public String getButton1() {
        return button1;
    }

    public String setButton1(String button1) {
        this.button1 = button1;
        return button1;
    }

    public String getHint() {
        return hint;
    }

    public String setHint(String hint) {
        this.hint = hint;
        return hint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }
}
