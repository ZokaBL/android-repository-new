package activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zoka.app.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import adapters.AdapterForSpinner;
import model.ObjectPageBean;


public class ObjectPageActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spin;

    ///MDBA/servlet?oper=objects&id=423&p1=&p2= - ovo je bilo prije ovoga dole

    private static final String URL_ZA_OBJECT_PAGE = "/MDBA/servlet?oper=objects&p1=";

   // private static final String URL_SAVE_BUTTON = "http://212.93.241.142:20282/MDBA/servlet?oper=dbobjsave&id=111&p1=2&p2=";
    private static final String URL_BACK_BUTTON = "http://212.93.241.142:20282/MDBA/servlet?oper=dbobjback&page=home&id=111&p1=&p2=";
    private static final String URL_CHANGE_BUTTON = "http://212.93.241.142:20282/MDBA/servlet?oper=dbobjchange&page=object&id=111&p1=&p2=";


    private static final String TAG = ObjectPageActivity.class.getSimpleName();


    String ipFromSharedPref;
    private SharedPreferences pref;

   // private static final String URL = "http://212.93.241.142:20282/MDBA/servlet?oper=dbobjsave&id=111&p1=2&p2=";

    private RequestQueue queue;
    int day, month, year;
    ImageView setDateBtn;

    Button buttonSaveFooter, buttonBackFooter, buttonChangeFooter;

    EditText editTextDatum;
    EditText editTextIme, editTextPrezime;
    AdapterForSpinner adapter;

    String ime;
    String prezime;
    String datum;


    private ArrayList<ObjectPageBean> dropDownList;
    ObjectPageBean body;

    String valueGotoPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.object_page_activity);

        pref = getSharedPreferences("ip", MODE_PRIVATE);
        ipFromSharedPref = pref.getString("ip","");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Object Page");
        setSupportActionBar(toolbar);
        queue = Volley.newRequestQueue(this);

        setDateBtn=(ImageView) findViewById(R.id.btn_date);
        setDateBtn.setOnClickListener(this);

        // loading json - Object Page
        loadingObjectPageActivity();
    }

    private void loadingObjectPageActivity(){
        dropDownList = new ArrayList<ObjectPageBean>();
        final JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, "http://" + ipFromSharedPref + URL_ZA_OBJECT_PAGE, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {

                    JSONArray arrayBody = response.getJSONArray("body");
                    for (int i=0; i<=response.length();i++) {

                        final ObjectPageBean objectPage = new ObjectPageBean();
                        // data from page
                       /* JSONObject objectPage1 = response.getJSONObject("page");
                        objectPage.setGotopage(objectPage1.getString("gotopage"));
                        valueGotoPage = objectPage1.getString("gotopage");

                       Toast.makeText(getBaseContext(), "Pokupio p1Value stranice: " + objectPage.getGotopage(), Toast.LENGTH_SHORT).show();
*/
                        // data from footer
                        JSONObject objectFooter = response.getJSONObject("footer");
                        objectPage.setP1Footer(objectFooter.getString("p1"));
                        objectPage.setP2Footer(objectFooter.getString("p2"));
                        objectPage.setFooterButton(objectFooter.getString("fbutton"));
                        objectPage.setFooterAction(objectFooter.getString("faction"));
                        objectPage.setPageName(objectFooter.getString("page"));


                        // BUTTON SAVE
                        buttonSaveFooter = (Button) findViewById(R.id.buttonSave);
                        String btnValueSave = objectFooter.getString("fbutton");

                        if (btnValueSave.equals(null)|| btnValueSave.equals("")){
                            buttonSaveFooter.setVisibility(View.INVISIBLE);

                        }else{
                            buttonSaveFooter.setVisibility(View.VISIBLE);
                            buttonSaveFooter.setText(objectFooter.getString("fbutton"));
                        }

                        buttonSaveFooter.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                editTextIme = (EditText) findViewById(R.id.edittextIme);
                                ime = String.valueOf(editTextIme.getText());
                                editTextPrezime = (EditText) findViewById(R.id.edittextPrezime);
                                prezime = String.valueOf(editTextPrezime.getText());
                                spin =(Spinner) findViewById(R.id.spinnerCustom);
                                String sex = String.valueOf(spin.getSelectedItem());
                                editTextDatum = (EditText) findViewById(R.id.edittextDatum);
                                datum = String.valueOf(editTextDatum.getText());


                                String URL_SAVE_PAGE = "http://" + ipFromSharedPref + URL_ZA_OBJECT_PAGE +
                                        "&first_name="+ ime.trim() +
                                        "&last_name=" + prezime.trim() +
                                        "&sex=" + sex + "&birthdate=" + datum ;


                                // readJson(URL_SAVE_PAGE);

                                GoToPage goToPage = new GoToPage(getBaseContext());
                                goToPage.readJson(URL_SAVE_PAGE);
                                finish();

                            }
                        });

                        // BUTTON CHANGE
                        buttonChangeFooter = (Button) findViewById(R.id.buttonChange);
                        String btnValueChange = "";

                        if (btnValueChange.equals(null)|| btnValueChange.equals("")){
                            buttonChangeFooter.setVisibility(View.INVISIBLE);

                        }else{
                            buttonChangeFooter.setVisibility(View.VISIBLE);
                            buttonChangeFooter.setText(objectFooter.getString("fbutton"));
                        }

                        buttonChangeFooter.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent change = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_CHANGE_BUTTON));
                                startActivity(change);
                            }
                        });


                        // BUTTON BACK

                        buttonBackFooter = (Button) findViewById(R.id.buttonBack);
                        String btnValueBack = " ";

                        if (btnValueBack.equals(null)|| btnValueBack.equals("")){
                            buttonBackFooter.setVisibility(View.INVISIBLE);

                        }else{
                            buttonBackFooter.setVisibility(View.VISIBLE);
                            buttonBackFooter.setText("Nazad");
                        }

                        buttonBackFooter.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent back = new Intent(Intent.ACTION_VIEW, Uri.parse(URL_BACK_BUTTON));
                                startActivity(back);
                            }
                        });


                        // data from header
                        JSONObject objectHeader = response.getJSONObject("header");
                        objectPage.setHeaderButton(objectHeader.getString("hbutton"));
                        objectPage.setP1Header(objectHeader.getString("p1"));
                        objectPage.setP2Header(objectHeader.getString("p2"));
                        objectPage.setHeaderHint(objectHeader.getString("hhint"));
                        objectPage.setHeaderCaption(objectHeader.getString("hcaption"));
                        objectPage.setHeaderAction(objectHeader.getString("haction"));
                        objectPage.setHeaderText(objectHeader.getString("htext"));


                        Button btnHeader = (Button) findViewById(R.id.buttonHeader);
                        String btnValue = objectHeader.getString("hbutton");

                        if (btnValue.equals(null)||btnValue.equals("")){
                            btnHeader.setVisibility(View.GONE);

                        }else{
                            btnHeader.setVisibility(View.VISIBLE);
                            btnHeader.setText(objectHeader.getString("hbutton"));
                        }



                        TextView headerText = (TextView) findViewById(R.id.headerText);
                        headerText.setText(objectHeader.getString("htext"));

                        if (headerText.equals(null)|| headerText.equals("")){
                            headerText.setVisibility(View.GONE);
                        }else {
                            headerText.setVisibility(View.VISIBLE);
                            headerText.setText(objectHeader.getString("htext"));

                        }


                        TextView headerCaption = (TextView) findViewById(R.id.headerCaption);
                        String hcapt = objectHeader.getString("hcaption");

                        if (hcapt.equals(null)|| hcapt.equals("")){
                            headerCaption.setVisibility(View.GONE);
                        }else {
                            headerCaption.setVisibility(View.VISIBLE);
                            headerCaption.setText(objectHeader.getString("hcaption"));

                        }

                        // data from body
                        for (int j=0; j<=arrayBody.length();j++) {
                            JSONObject object2 = arrayBody.getJSONObject(j);

                            if (j==1) {
                                editTextIme = (EditText) findViewById(R.id.edittextIme);
                                String hintIme = object2.getString("hint");
                                editTextIme.setHint(hintIme);
                                editTextIme.setVisibility(View.VISIBLE);

                            }

                            if (j==2) {
                                editTextPrezime = (EditText) findViewById(R.id.edittextPrezime);
                                String hintPrezime = object2.getString("hint");
                               editTextPrezime.setHint(hintPrezime);
                                editTextPrezime.setVisibility(View.VISIBLE);
                            }

                            editTextDatum = (EditText) findViewById(R.id.edittextDatum);
                            String hintDatum = object2.getString("hint");
                            editTextDatum.setHint(hintDatum);
                            editTextDatum.setVisibility(View.VISIBLE);

                            JSONArray arrayList = object2.getJSONArray("list");
                            for (int n = 0; n<arrayList.length(); n++) {
                                JSONObject object3 = arrayList.getJSONObject(n);
                                body = new ObjectPageBean();
                                String id = body.setID(object3.getString("listvalue"));
                                String name = body.setNameDataBase(object3.getString("listname"));
                                body = new ObjectPageBean(id,name);
                                dropDownList.add(body);
                                spin = (Spinner) findViewById(R.id.spinnerCustom);
                                spin.setVisibility(View.VISIBLE);
                                ImageView img = (ImageView) findViewById(R.id.btn_date);
                                img.setVisibility(View.VISIBLE);
                                adapter = new AdapterForSpinner(getBaseContext(), dropDownList);
                                spin.setAdapter(adapter);
                                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                       // String item = parent.getItemAtPosition(position).toString();

                                        String idSelected = dropDownList.get(position).getID();

                                        if (idSelected.equals(dropDownList.get(position).getID())){
                                            //Toast.makeText(parent.getContext(), "listvalue: " + dropDownList.get(position).getID(), Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }

    // metoda prima url do json i provjerava koja je vrednost gotopage i u zavisnosti od toga ide na tu stranicu
    private void readJson(String url) {
        final JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                        JSONObject objectPage = response.getJSONObject("page");
                        valueGotoPage = objectPage.getString("gotopage");


                    if (valueGotoPage.equals("home")){
                        Intent homePage = new Intent(getBaseContext(),MainActivity.class);
                        startActivity(homePage);
                        finish();
                    }
                    if (valueGotoPage.equals("object")){
                        Intent object = new Intent(getBaseContext(),ObjectPageActivity.class);
                        startActivity(object);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent objectPage = new Intent(ObjectPageActivity.this, MainActivity.class);
        objectPage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(objectPage);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v == setDateBtn) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            editTextDatum.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);

                        }
                    }, year, month, day);
            datePickerDialog.show();
        }
    }
}

